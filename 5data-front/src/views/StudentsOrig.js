import React from "react"
import { ResponsiveBubble } from '@nivo/circle-packing'
import { ResponsiveBar } from '@nivo/bar'
import { ResponsivePie } from '@nivo/pie'
import { Col, Card, CardTitle, Row } from "reactstrap"
import { FaMapMarkedAlt } from 'react-icons/fa'

import AxiosHelp from "../tools/AxiosHelp"

class StudentsOrig extends React.Component {


    // [
    //     {
    //       "id": "stylus",
    //       "label": "stylus",
    //       "value": 471,
    //       "color": "hsl(11, 70%, 50%)"
    //     },
    //     {
    //       "id": "php",
    //       "label": "php",
    //       "value": 72,
    //       "color": "hsl(278, 70%, 50%)"
    //     },
    //     {
    //       "id": "hack",
    //       "label": "hack",
    //       "value": 514,
    //       "color": "hsl(14, 70%, 50%)"
    //     },
    //     {
    //       "id": "rust",
    //       "label": "rust",
    //       "value": 182,
    //       "color": "hsl(99, 70%, 50%)"
    //     },
    //     {
    //       "id": "make",
    //       "label": "make",
    //       "value": 569,
    //       "color": "hsl(70, 70%, 50%)"
    //     }
    //   ]
    state = {
        top10: [],
        regionRepart: []
    }

    componentDidMount() {
        this.loadData()
    }


    loadData() {

        AxiosHelp.axios().get("/api/data/regionRepartition")
            .then(response => {

                let finalData = response.data.nbEtudiantsParRegion.map(r => {
                    return {
                        "id": r.nomRegion,
                        "label": r.nomRegion,
                        "value": r.valeurPourCetteRegion,
                    }
                })

                this.setState({
                    regionRepart: finalData
                })
            });

        // =======================================

        AxiosHelp.axios().get("/api/data/topSchool")
            .then(response => {

                let finalData = response.data.nbEtudiantsParEcole.map(r => {
                    return {
                        schoolname: r.nomEcole,
                        students: r.valeurPourCetteEcole
                    }
                })

                this.setState({
                    top10: finalData
                })
            });
    }


    render() {
        return <React.Fragment>
            <div className="text-center mt-4">
                <h1 className="mb-5">< FaMapMarkedAlt size={100} /><br />Les étudiants et leur localisation</h1>
                <Row>
                    <Col md={{ size: 5, offset: 1 }} >
                        <h2>Répartition régionale</h2>

                        <div className="chart whitechart">
                            <ResponsivePie
                                data={this.state.regionRepart}
                                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                                innerRadius={0.5}
                                padAngle={0.7}
                                cornerRadius={3}
                                colors={{ scheme: 'nivo' }}
                                borderWidth={1}
                                borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
                                radialLabelsSkipAngle={10}
                                radialLabelsTextColor="#333333"
                                radialLabelsLinkColor={{ from: 'color' }}
                                sliceLabelsSkipAngle={10}
                                sliceLabelsTextColor="#333333"
                                defs={[
                                    {
                                        id: 'dots',
                                        type: 'patternDots',
                                        background: 'inherit',
                                        color: 'rgba(255, 255, 255, 0.3)',
                                        size: 4,
                                        padding: 1,
                                        stagger: true
                                    },
                                    {
                                        id: 'lines',
                                        type: 'patternLines',
                                        background: 'inherit',
                                        color: 'rgba(255, 255, 255, 0.3)',
                                        rotation: -45,
                                        lineWidth: 6,
                                        spacing: 10
                                    }
                                ]}
                                fill={[
                                    {
                                        match: {
                                            id: 'ruby'
                                        },
                                        id: 'dots'
                                    },
                                    {
                                        match: {
                                            id: 'c'
                                        },
                                        id: 'dots'
                                    },
                                    {
                                        match: {
                                            id: 'go'
                                        },
                                        id: 'dots'
                                    },
                                    {
                                        match: {
                                            id: 'python'
                                        },
                                        id: 'dots'
                                    },
                                    {
                                        match: {
                                            id: 'scala'
                                        },
                                        id: 'lines'
                                    },
                                    {
                                        match: {
                                            id: 'lisp'
                                        },
                                        id: 'lines'
                                    },
                                    {
                                        match: {
                                            id: 'elixir'
                                        },
                                        id: 'lines'
                                    },
                                    {
                                        match: {
                                            id: 'javascript'
                                        },
                                        id: 'lines'
                                    }
                                ]}
                                legends={[
                                    {
                                        anchor: 'bottom',
                                        direction: 'row',
                                        justify: false,
                                        translateX: 0,
                                        translateY: 56,
                                        itemsSpacing: 0,
                                        itemWidth: 100,
                                        itemHeight: 18,
                                        itemTextColor: '#999',
                                        itemDirection: 'left-to-right',
                                        itemOpacity: 1,
                                        symbolSize: 18,
                                        symbolShape: 'circle',
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemTextColor: '#000'
                                                }
                                            }
                                        ]
                                    }
                                ]}
                            />
                        </div>
                        <Card body color="danger">
                            Comme nous pouvons le remarquer le nombre d'étudiants dans notre jeu de donnée est équilbré entre chacune des régions. Cependant on observe une légère prévalence 
                            pour la californie, la floride et l'état de new york.
                    </Card>
                    </Col>

                    <Col md={{ size: 4, offset: 1 }}>
                        <h2>Top 10 écoles</h2>

                        <div className="chart whitechart">
                            <ResponsiveBar
                                animate={false}
                                data={this.state.top10}
                                keys={['students']}
                                indexBy="schoolname"
                                margin={{ top: 50, right: 0, bottom: 50, left: 60 }}
                                padding={0.3}
                                valueScale={{ type: 'linear' }}
                                indexScale={{ type: 'band', round: true }}
                                colors={{ scheme: 'nivo' }}
                                borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                                axisTop={null}
                                axisRight={null}
                                axisBottom={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'Nom des écoles',
                                    legendPosition: 'middle',
                                    legendOffset: 32
                                }}
                                axisLeft={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: "quantité d'étudiant",
                                    legendPosition: 'middle',
                                    legendOffset: -40
                                }}
                                labelSkipWidth={12}
                                labelSkipHeight={12}
                                labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}

                                animate={true}
                                motionStiffness={90}
                                motionDamping={15}
                            />
                        </div>
                        <Card body color="danger">
                            Nous remarquons que l'école la plus peuplée est supinfo devant epitech et epita.
                    </Card>
                    </Col>
                </Row>


            </div>



        </React.Fragment>
    }
}
export default StudentsOrig


const fakeOrigins = {
    "name": "students",
    "children": [
        {
            "name": "region1",
            "children": [
                {
                    "name": "school-1",
                    "studentsquantity": 10000
                },
                {
                    "name": "school-2",
                    "studentsquantity": 10000
                }
            ]
        },
        {
            "name": "region2",
            "children": [
                {
                    "name": "school-2",
                    "studentsquantity": 5000
                },
                {
                    "name": "school-3",
                    "studentsquantity": 2000
                }
            ]
        },

    ]
}