import React from "react"
import {
    ComposableMap,
    Geographies,
    Geography,
    ZoomableGroup,
    Marker
} from "react-simple-maps";

import { Col, Card, CardTitle, Row } from "reactstrap"
import CountUp from 'react-countup';

import { FaUserGraduate,FaSchool,FaMapMarkedAlt } from 'react-icons/fa'

import AxiosHelp from "../tools/AxiosHelp"

const geoUrl =
    "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

class StudentsMap extends React.Component {

    state = {
        studentsCount : 0,
        schoolCount: 0,
        regionCount: 0
    }

    componentDidMount(){
        this.loadData()
    }

    render() {
        return <div className="mt-5 p-5" style={{ backgroundColor: '#007bffa1' }}>
            <Col md={{ size: 8, offset: 2 }} >
                <h1 className="text-center">Nos étudiants à travers le monde</h1>
                <ComposableMap height={400}>
                    <ZoomableGroup zoom={3} center={[-98, 38]}>
                        <Geographies geography={geoUrl}>
                            {({ geographies }) =>
                                geographies.map(geo => (
                                    <Geography key={geo.rsmKey} geography={geo} />
                                ))
                            }
                        </Geographies>

                        <Marker coordinates={[-110.006, 31.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-80.006, 41.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-72.006, 42.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-71.006, 42.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-75.006, 45.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-74.006, 48.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-76.006, 49.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-72.006, 42.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-71.006, 47.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-98.006, 40.7128]}><circle r={1} fill="#F53" /></Marker>
                        <Marker coordinates={[-98.006, 25.7128]}><circle r={1} fill="#F53" /></Marker>
                    </ZoomableGroup>
                </ComposableMap>
            </Col>
            <Row  className="mt-5 text-center">
                <Col md={4}>
                    <Card body color="info">
                        <CardTitle tag="h5"><FaUserGraduate size={50} /><br />Etudiants</CardTitle>
                        <div className="counter">
                            <CountUp end={this.state.studentsCount} duration={3} separator=" " />
                        </div>
                        Représente la quantité totale de nos étudiants dans le monde
                    </Card>
                </Col>
                <Col md={4}>
                    <Card body color="info">
                        <CardTitle tag="h5"><FaSchool size={50} /><br />Ecoles</CardTitle>
                        <div className="counter">
                            <CountUp end={this.state.schoolCount} duration={3} separator=" " />
                        </div>
                        Quantité d'école dans nos données
                    </Card>
                </Col>
                <Col md={4}>
                    <Card body color="info">
                        <CardTitle tag="h5"><FaMapMarkedAlt size={50} /><br />Régions</CardTitle>
                        <div className="counter">
                            <CountUp end={this.state.regionCount} duration={3} separator=" " />
                        </div>
                        Toutes les régions que nous décomptons
                    </Card>
                </Col>
            </Row>
        </div>
    }


    loadData() {
        AxiosHelp.axios().get("/api/data/studentsArroundWorld")
            .then(response => {
                this.setState({
                    studentsCount: response.data.nbEtudiants,
                    schoolCount: response.data.nbEcoles,
                    regionCount:response.data.nbRegions,
                })
            });
    }

} export default StudentsMap