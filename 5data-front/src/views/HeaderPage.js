import React from 'react'
import { Jumbotron, Table, Col, Media } from 'reactstrap'

import { IoMdPerson } from 'react-icons/io'
import BigdataImg from '../assets/big-data.jpg'


class HeaderPage extends React.Component {

    render() {
        return <React.Fragment>
            <Col lg="12" md="12" sm="12" className = "headerImg">
                <Media src={BigdataImg}/>
            </Col>

            <Jumbotron style={{ "backgroundColor": "#00000000"  }}>
                <h1 className="display-3 mt-3">5DATA</h1>
                <p className="lead mt-3">Projet d'analyse de données en big data</p>
        
                <p>Exploitation du catalogue de données nationale des étudiants</p>

                <Col lg="3" md="4" xs="12">
                    <Table className="invisibleTab">
                        <thead>
                            <tr>
                                <th>
                                    <IoMdPerson size={25} className="mr-2" />Auteurs
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>HAUDUC Némo - 305903</td>
                            </tr>
                            <tr>
                                <td> LARIBIERE Bruno - 298261</td>
                            </tr>


                        </tbody>
                    </Table>
                </Col>



            </Jumbotron>

        </React.Fragment>
    }

}
export default HeaderPage