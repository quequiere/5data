import React from "react"

import { FaUserGraduate } from 'react-icons/fa'
import { Col, Card, CardTitle, Row } from "reactstrap"

import { ResponsiveFunnel } from '@nivo/funnel'
import { ResponsiveLine } from '@nivo/line'
import { ResponsiveSwarmPlot } from '@nivo/swarmplot'
import { ResponsiveBar } from '@nivo/bar'

import AxiosHelp from "../tools/AxiosHelp"



class Grade extends React.Component {

    state = {
        //empty data to avoid load problems
        markQuantity: [{
            "id": "G1",
            "value": 0,
            "label": "0"
        }],

        markBySex: [],

        regionDataStudents : [],

        chanceType : [],
    }


    componentDidMount() {
        this.loadData()
    }


    loadData() {

        AxiosHelp.axios().get("/api/data/chanbeBySchoolType")
        .then(response => {

            let finalData =  [
                {
                    "schooltype": "Privé",
                    "success": response.data.successPrive,
                    "failed": response.data.failedPrive,
                },
                {
                    "schooltype": "Publique",
                    "success": response.data.successPublic,
                    "failed": response.data.failedPublic,
                }
            
            ]



            this.setState({
                chanceType: finalData
            })
        });

          // ================================================

        AxiosHelp.axios().get("/api/data/markByRegion")
        .then(response => {

            let dataGroup = response.data.map( r=> {
                return {
                    id:r.id,
                    regioname:r.regionName,
                    group: r.mark,
                    quantity : r.quantity,
                    volume: 20
                }
            })

            this.setState({
                regionDataStudents: dataGroup
            })
        });


        
        // ================================================

        AxiosHelp.axios().get("/api/data/markbySex")
            .then(response => {

                let finalData = [
                    {
                        "id": "Garcons",
                        "data": [
                            {
                                "x": "0 à 4",
                                "y": response.data.zeroAQuatreGarcon
                            },
                            {
                                "x": "5 à 8",
                                "y": response.data.cinqAHuitGarcon
                            },
                            {
                                "x": "9 à 12",
                                "y": response.data.neufADouzeGarcon
                            },
                            {
                                "x": "13 à 16",
                                "y": response.data.treizeASeizeGarcon
                            },
                            {
                                "x": "17 à 20",
                                "y": response.data.dixseptAVingtGarcon
                            }
                        ]
                    }, {
                        "id": "Filles",
                        "data": [
                            {
                                "x": "0 à 4",
                                "y": response.data.zeroAQuatreFille
                            },
                            {
                                "x": "5 à 8",
                                "y": response.data.cinqAHuitFille
                            },
                            {
                                "x": "9 à 12",
                                "y": response.data.neufADouzeFille
                            },
                            {
                                "x": "13 à 16",
                                "y": response.data.treizeASeizeFille
                            },
                            {
                                "x": "17 à 20",
                                "y": response.data.dixseptAVingtFille
                            }
                        ]
                    },
                ]

                this.setState({
                    markBySex: finalData
                })
            });









        // ================================================

        AxiosHelp.axios().get("/api/data/markQuantity")
            .then(response => {

                let finalData = [{
                    "id": "G1",
                    "value": response.data.zeroAQuatre,
                    "label": "0 à 4"
                },
                {
                    "id": "G2",
                    "value": response.data.cinqAHuit,
                    "label": "5 à 8"
                },
                {
                    "id": "G3",
                    "value": response.data.neufADouze,
                    "label": "9 à 12"
                },
                {
                    "id": "G4",
                    "value": response.data.treizeASeize,
                    "label": "13 à 16"
                },
                {
                    "id": "G5",
                    "value": response.data.dixseptAVingt,
                    "label": "17 à 20"
                }]

                this.setState({
                    markQuantity: finalData
                })
            });
    }


    render() {
        return <div className="text-center mt-5">
            <hr />
            <h1 className="mb-5">< FaUserGraduate size={100} /><br />Les notes et les étudiants</h1>
            <Col md={{ size: 4, offset: 4 }}>
                <Card body color="success">
                    Nous allons dans cette partie faire une analyse plus poussée des notes des élèves. Nous pourrons faire une analyse de la répartions des notes.
                </Card>
            </Col>
            <Row className="mt-5">
                <Col md={6}>
                    <h2>Répartition quantitative des notes</h2>
                    <div className="chart" >
                        <ResponsiveFunnel
                            data={this.state.markQuantity}
                            margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
                            valueFormat=""
                            colors={{ scheme: 'spectral' }}
                            borderWidth={20}
                            labelColor={{ from: 'color', modifiers: [['darker', 3]] }}
                            beforeSeparatorLength={100}
                            beforeSeparatorOffset={20}
                            afterSeparatorLength={100}
                            afterSeparatorOffset={20}
                            currentPartSizeExtension={10}
                            currentBorderWidth={40}
                            motionConfig="wobbly"
                        />
                    </div>
                    <Col md={{ size: 6, offset: 3 }}>

                        <Card body color="primary">
                            Nous obervons que les notes se répartissent majoritairement entre 9 et 12.<br/>
                            Les notes très mauvaises de 0 à 4 sont moins nombreuses que les notes excellentes de 17 à 20.
                        
                        </Card>
                    </Col>
                </Col>

                <Col md={6}>
                    <h2>Répartition des notes en fonction du sexe</h2>
                    <div className="chart whitechart" >
                        <ResponsiveLine
                            animate={false}
                            data={this.state.markBySex}
                            margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
                            xScale={{ type: 'point' }}
                            yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: false, reverse: false }}
                            yFormat=" >-.2f"
                            axisTop={null}
                            axisRight={null}
                            axisBottom={{
                                orient: 'bottom',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Note',
                                legendOffset: 36,
                                legendPosition: 'middle'
                            }}
                            axisLeft={{
                                orient: 'left',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: "Quantité d'élève",
                                legendOffset: -40,
                                legendPosition: 'middle'
                            }}
                            pointSize={10}
                            pointColor={{ theme: 'background' }}
                            pointBorderWidth={2}
                            pointBorderColor={{ from: 'serieColor' }}
                            pointLabelYOffset={-12}
                            useMesh={true}
                            legends={[
                                {
                                    anchor: 'bottom-right',
                                    direction: 'column',
                                    justify: false,
                                    translateX: 100,
                                    translateY: 0,
                                    itemsSpacing: 0,
                                    itemDirection: 'left-to-right',
                                    itemWidth: 80,
                                    itemHeight: 20,
                                    itemOpacity: 0.75,
                                    symbolSize: 12,
                                    symbolShape: 'circle',
                                    symbolBorderColor: 'rgba(0, 0, 0, .5)',
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemBackground: 'rgba(0, 0, 0, .03)',
                                                itemOpacity: 1
                                            }
                                        }
                                    ]
                                }
                            ]}
                        />
                    </div>
                    <Col md={{ size: 6, offset: 3 }}>

                        <Card body color="primary">
                            D'après ce graphique il y a une corrélation entre les notes des filles et des garçons.<br/>
                            Le fait d'être un garçon ou une fille ne semble donc pas avoir d'impacte sur la scolarité
                    </Card>

                    </Col>
                </Col>


            </Row>

            <Row className="mt-5">
                <Col md={6}>
                    <h2>Répartition des notes par régions</h2>
                    <div className="chart whitechart" >
                        <ResponsiveSwarmPlot
                            animate={false}
                            label="id"
                            data={this.state.regionDataStudents}
                            groups={['G1', 'G2', 'G3', 'G4', 'G5']}
                            value="quantity"
                            valueFormat=""
                            valueScale={{ type: 'linear', min: 0, max: 700, reverse: false }}
                            size={{ key: 'volume', values: [4, 20], sizes: [6, 20] }}
                            forceStrength={4}
                            simulationIterations={100}
                            margin={{ top: 80, right: 100, bottom: 80, left: 100 }}
                            axisBottom={{
                                orient: 'bottom',
                                tickSize: 10,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Notes des étudiants',
                                legendPosition: 'middle',
                                legendOffset: 46
                            }}
                            axisLeft={{
                                orient: 'left',
                                tickSize: 10,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Quantité d étudiants',
                                legendPosition: 'middle',
                                legendOffset: -76
                            }}
                            motionStiffness={50}
                            motionDamping={10}
                        />
                    </div>
                    <Col md={{ size: 6, offset: 3 }}>

                        <Card body color="primary">
                            Sur ce graphique nous représentons la disparité des élèves en fonction des notes parrégions. Les notes sont notées de G1 à G5 pour un affichage simplifié<br/>
                            On peut remarquer que la plus grande disparité se retrouve à l'approche de la moyenne globale.
                        </Card>
                    </Col>

                </Col>


                <Col md={6}>
                    <h2>Evaluation des chances par type d'école</h2>
                    <div className="chart whitechart" >
                        <ResponsiveBar
                            animate={false}
                            data={this.state.chanceType}
                            keys={['failed', 'success']}
                            indexBy="schooltype"
                            margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
                            padding={0.3}
                            valueScale={{ type: 'linear' }}
                            indexScale={{ type: 'band', round: true }}
                            colors={{ scheme: 'nivo' }}
                            borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                            axisTop={null}
                            axisRight={null}
                            axisBottom={{
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: "Type d'école",
                                legendPosition: 'middle',
                                legendOffset: 32
                            }}
                            axisLeft={{
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'quantité d elève',
                                legendPosition: 'middle',
                                legendOffset: -40
                            }}
                            labelSkipWidth={12}
                            labelSkipHeight={12}
                            labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                            legends={[
                                {
                                    dataFrom: 'keys',
                                    anchor: 'bottom-right',
                                    direction: 'column',
                                    justify: false,
                                    translateX: 120,
                                    translateY: 0,
                                    itemsSpacing: 2,
                                    itemWidth: 100,
                                    itemHeight: 20,
                                    itemDirection: 'left-to-right',
                                    itemOpacity: 0.85,
                                    symbolSize: 20,
                                    effects: [
                                        {
                                            on: 'hover',
                                            style: {
                                                itemOpacity: 1
                                            }
                                        }
                                    ]
                                }
                            ]}
                            animate={true}
                            motionStiffness={90}
                            motionDamping={15}
                        />
                    </div>
                    <Col md={{ size: 6, offset: 3 }}>

                        <Card body color="primary">
                            D'après ce graphique et notre jeu de donnée nous pouvons avancer que les chances dans le publique et le privé sont les mêmes.<br/>
                            Le ratio d'échec VS réussite est sensisblement le même.
                        </Card>
                    </Col>

                </Col>

            </Row>


        </div>
    }

} export default Grade


const regionData = [
    {
        "id": "0.0",
        "group": "group A",
        "price": 291,
        "volume": 4
    },
    {
        "id": "0.1",
        "group": "group A",
        "price": 335,
        "volume": 20
    },
    {
        "id": "0.2",
        "group": "group A",
        "price": 334,
        "volume": 10
    },
    {
        "id": "0.3",
        "group": "group A",
        "price": 17,
        "volume": 19
    },
    {
        "id": "0.4",
        "group": "group A",
        "price": 3,
        "volume": 5
    },
    {
        "id": "0.5",
        "group": "group A",
        "price": 251,
        "volume": 7
    },
    {
        "id": "0.6",
        "group": "group A",
        "price": 118,
        "volume": 8
    },
    {
        "id": "0.7",
        "group": "group A",
        "price": 0,
        "volume": 16
    },
    {
        "id": "0.8",
        "group": "group A",
        "price": 425,
        "volume": 9
    },
    {
        "id": "0.9",
        "group": "group A",
        "price": 327,
        "volume": 9
    },
    {
        "id": "0.10",
        "group": "group A",
        "price": 404,
        "volume": 14
    },
    {
        "id": "0.11",
        "group": "group A",
        "price": 454,
        "volume": 5
    },
    {
        "id": "0.12",
        "group": "group A",
        "price": 448,
        "volume": 5
    },
    {
        "id": "0.13",
        "group": "group A",
        "price": 259,
        "volume": 10
    },
    {
        "id": "0.14",
        "group": "group A",
        "price": 156,
        "volume": 12
    },
    {
        "id": "0.15",
        "group": "group A",
        "price": 315,
        "volume": 15
    },
    {
        "id": "0.16",
        "group": "group A",
        "price": 244,
        "volume": 19
    },
    {
        "id": "0.17",
        "group": "group A",
        "price": 43,
        "volume": 13
    },
    {
        "id": "0.18",
        "group": "group A",
        "price": 432,
        "volume": 8
    },
    {
        "id": "0.19",
        "group": "group A",
        "price": 37,
        "volume": 9
    },
    {
        "id": "0.20",
        "group": "group A",
        "price": 329,
        "volume": 7
    },
    {
        "id": "0.21",
        "group": "group A",
        "price": 390,
        "volume": 5
    },
    {
        "id": "0.22",
        "group": "group A",
        "price": 132,
        "volume": 10
    },
    {
        "id": "0.23",
        "group": "group A",
        "price": 285,
        "volume": 6
    },
    {
        "id": "0.24",
        "group": "group A",
        "price": 325,
        "volume": 13
    },
    {
        "id": "0.25",
        "group": "group A",
        "price": 390,
        "volume": 17
    },
    {
        "id": "0.26",
        "group": "group A",
        "price": 96,
        "volume": 20
    },
    {
        "id": "0.27",
        "group": "group A",
        "price": 160,
        "volume": 12
    },
    {
        "id": "0.28",
        "group": "group A",
        "price": 367,
        "volume": 16
    },
    {
        "id": "0.29",
        "group": "group A",
        "price": 383,
        "volume": 10
    },
    {
        "id": "0.30",
        "group": "group A",
        "price": 231,
        "volume": 12
    },
    {
        "id": "0.31",
        "group": "group A",
        "price": 447,
        "volume": 5
    },
    {
        "id": "0.32",
        "group": "group A",
        "price": 7,
        "volume": 4
    },
    {
        "id": "0.33",
        "group": "group A",
        "price": 124,
        "volume": 8
    },
    {
        "id": "0.34",
        "group": "group A",
        "price": 380,
        "volume": 4
    },
    {
        "id": "0.35",
        "group": "group A",
        "price": 470,
        "volume": 7
    },
    {
        "id": "0.36",
        "group": "group A",
        "price": 391,
        "volume": 13
    },
    {
        "id": "0.37",
        "group": "group A",
        "price": 325,
        "volume": 15
    },
    {
        "id": "0.38",
        "group": "group A",
        "price": 54,
        "volume": 14
    },
    {
        "id": "0.39",
        "group": "group A",
        "price": 367,
        "volume": 15
    },
    {
        "id": "0.40",
        "group": "group A",
        "price": 388,
        "volume": 20
    },
    {
        "id": "0.41",
        "group": "group A",
        "price": 2,
        "volume": 20
    },
    {
        "id": "0.42",
        "group": "group A",
        "price": 55,
        "volume": 10
    },
    {
        "id": "0.43",
        "group": "group A",
        "price": 316,
        "volume": 5
    },
    {
        "id": "0.44",
        "group": "group A",
        "price": 378,
        "volume": 7
    },
    {
        "id": "0.45",
        "group": "group A",
        "price": 262,
        "volume": 16
    },
    {
        "id": "0.46",
        "group": "group A",
        "price": 123,
        "volume": 20
    },
    {
        "id": "0.47",
        "group": "group A",
        "price": 378,
        "volume": 16
    },
    {
        "id": "0.48",
        "group": "group A",
        "price": 396,
        "volume": 7
    },
    {
        "id": "0.49",
        "group": "group A",
        "price": 290,
        "volume": 11
    },
    {
        "id": "0.50",
        "group": "group A",
        "price": 230,
        "volume": 8
    },
    {
        "id": "1.0",
        "group": "group B",
        "price": 415,
        "volume": 8
    },
    {
        "id": "1.1",
        "group": "group B",
        "price": 342,
        "volume": 4
    },
    {
        "id": "1.2",
        "group": "group B",
        "price": 14,
        "volume": 4
    },
    {
        "id": "1.3",
        "group": "group B",
        "price": 97,
        "volume": 4
    },
    {
        "id": "1.4",
        "group": "group B",
        "price": 74,
        "volume": 8
    },
    {
        "id": "1.5",
        "group": "group B",
        "price": 286,
        "volume": 9
    },
    {
        "id": "1.6",
        "group": "group B",
        "price": 286,
        "volume": 15
    },
    {
        "id": "1.7",
        "group": "group B",
        "price": 446,
        "volume": 16
    },
    {
        "id": "1.8",
        "group": "group B",
        "price": 478,
        "volume": 11
    },
    {
        "id": "1.9",
        "group": "group B",
        "price": 350,
        "volume": 15
    },
    {
        "id": "1.10",
        "group": "group B",
        "price": 192,
        "volume": 6
    },
    {
        "id": "1.11",
        "group": "group B",
        "price": 165,
        "volume": 8
    },
    {
        "id": "1.12",
        "group": "group B",
        "price": 396,
        "volume": 11
    },
    {
        "id": "1.13",
        "group": "group B",
        "price": 166,
        "volume": 8
    },
    {
        "id": "1.14",
        "group": "group B",
        "price": 376,
        "volume": 7
    },
    {
        "id": "1.15",
        "group": "group B",
        "price": 306,
        "volume": 15
    },
    {
        "id": "1.16",
        "group": "group B",
        "price": 450,
        "volume": 19
    },
    {
        "id": "1.17",
        "group": "group B",
        "price": 35,
        "volume": 13
    },
    {
        "id": "1.18",
        "group": "group B",
        "price": 237,
        "volume": 7
    },
    {
        "id": "1.19",
        "group": "group B",
        "price": 278,
        "volume": 10
    },
    {
        "id": "1.20",
        "group": "group B",
        "price": 197,
        "volume": 10
    },
    {
        "id": "1.21",
        "group": "group B",
        "price": 367,
        "volume": 20
    },
    {
        "id": "1.22",
        "group": "group B",
        "price": 446,
        "volume": 12
    },
    {
        "id": "1.23",
        "group": "group B",
        "price": 485,
        "volume": 8
    },
    {
        "id": "1.24",
        "group": "group B",
        "price": 160,
        "volume": 15
    },
    {
        "id": "1.25",
        "group": "group B",
        "price": 475,
        "volume": 20
    },
    {
        "id": "1.26",
        "group": "group B",
        "price": 448,
        "volume": 11
    },
    {
        "id": "1.27",
        "group": "group B",
        "price": 495,
        "volume": 12
    },
    {
        "id": "1.28",
        "group": "group B",
        "price": 214,
        "volume": 18
    },
    {
        "id": "1.29",
        "group": "group B",
        "price": 242,
        "volume": 16
    },
    {
        "id": "1.30",
        "group": "group B",
        "price": 214,
        "volume": 16
    },
    {
        "id": "1.31",
        "group": "group B",
        "price": 260,
        "volume": 12
    },
    {
        "id": "1.32",
        "group": "group B",
        "price": 209,
        "volume": 5
    },
    {
        "id": "1.33",
        "group": "group B",
        "price": 143,
        "volume": 8
    },
    {
        "id": "1.34",
        "group": "group B",
        "price": 499,
        "volume": 20
    },
    {
        "id": "1.35",
        "group": "group B",
        "price": 102,
        "volume": 11
    },
    {
        "id": "1.36",
        "group": "group B",
        "price": 481,
        "volume": 17
    },
    {
        "id": "1.37",
        "group": "group B",
        "price": 191,
        "volume": 8
    },
    {
        "id": "1.38",
        "group": "group B",
        "price": 113,
        "volume": 19
    },
    {
        "id": "1.39",
        "group": "group B",
        "price": 201,
        "volume": 17
    },
    {
        "id": "1.40",
        "group": "group B",
        "price": 177,
        "volume": 4
    },
    {
        "id": "1.41",
        "group": "group B",
        "price": 258,
        "volume": 6
    },
    {
        "id": "1.42",
        "group": "group B",
        "price": 314,
        "volume": 12
    },
    {
        "id": "1.43",
        "group": "group B",
        "price": 157,
        "volume": 11
    },
    {
        "id": "1.44",
        "group": "group B",
        "price": 199,
        "volume": 10
    },
    {
        "id": "1.45",
        "group": "group B",
        "price": 131,
        "volume": 6
    },
    {
        "id": "1.46",
        "group": "group B",
        "price": 325,
        "volume": 7
    },
    {
        "id": "1.47",
        "group": "group B",
        "price": 497,
        "volume": 5
    },
    {
        "id": "1.48",
        "group": "group B",
        "price": 172,
        "volume": 6
    },
    {
        "id": "1.49",
        "group": "group B",
        "price": 150,
        "volume": 13
    },
    {
        "id": "1.50",
        "group": "group B",
        "price": 133,
        "volume": 14
    },
    {
        "id": "1.51",
        "group": "group B",
        "price": 273,
        "volume": 12
    },
    {
        "id": "1.52",
        "group": "group B",
        "price": 90,
        "volume": 9
    },
    {
        "id": "1.53",
        "group": "group B",
        "price": 237,
        "volume": 8
    },
    {
        "id": "1.54",
        "group": "group B",
        "price": 487,
        "volume": 13
    },
    {
        "id": "2.0",
        "group": "group C",
        "price": 464,
        "volume": 14
    },
    {
        "id": "2.1",
        "group": "group C",
        "price": 70,
        "volume": 15
    },
    {
        "id": "2.2",
        "group": "group C",
        "price": 365,
        "volume": 12
    },
    {
        "id": "2.3",
        "group": "group C",
        "price": 321,
        "volume": 11
    },
    {
        "id": "2.4",
        "group": "group C",
        "price": 166,
        "volume": 7
    },
    {
        "id": "2.5",
        "group": "group C",
        "price": 212,
        "volume": 12
    },
    {
        "id": "2.6",
        "group": "group C",
        "price": 266,
        "volume": 8
    },
    {
        "id": "2.7",
        "group": "group C",
        "price": 25,
        "volume": 20
    },
    {
        "id": "2.8",
        "group": "group C",
        "price": 132,
        "volume": 12
    },
    {
        "id": "2.9",
        "group": "group C",
        "price": 229,
        "volume": 16
    },
    {
        "id": "2.10",
        "group": "group C",
        "price": 442,
        "volume": 7
    },
    {
        "id": "2.11",
        "group": "group C",
        "price": 386,
        "volume": 18
    },
    {
        "id": "2.12",
        "group": "group C",
        "price": 358,
        "volume": 15
    },
    {
        "id": "2.13",
        "group": "group C",
        "price": 345,
        "volume": 7
    },
    {
        "id": "2.14",
        "group": "group C",
        "price": 108,
        "volume": 16
    },
    {
        "id": "2.15",
        "group": "group C",
        "price": 27,
        "volume": 8
    },
    {
        "id": "2.16",
        "group": "group C",
        "price": 256,
        "volume": 8
    },
    {
        "id": "2.17",
        "group": "group C",
        "price": 8,
        "volume": 15
    },
    {
        "id": "2.18",
        "group": "group C",
        "price": 7,
        "volume": 12
    },
    {
        "id": "2.19",
        "group": "group C",
        "price": 300,
        "volume": 4
    },
    {
        "id": "2.20",
        "group": "group C",
        "price": 246,
        "volume": 18
    },
    {
        "id": "2.21",
        "group": "group C",
        "price": 344,
        "volume": 11
    },
    {
        "id": "2.22",
        "group": "group C",
        "price": 141,
        "volume": 8
    },
    {
        "id": "2.23",
        "group": "group C",
        "price": 190,
        "volume": 20
    },
    {
        "id": "2.24",
        "group": "group C",
        "price": 357,
        "volume": 9
    },
    {
        "id": "2.25",
        "group": "group C",
        "price": 333,
        "volume": 9
    },
    {
        "id": "2.26",
        "group": "group C",
        "price": 55,
        "volume": 11
    },
    {
        "id": "2.27",
        "group": "group C",
        "price": 464,
        "volume": 16
    },
    {
        "id": "2.28",
        "group": "group C",
        "price": 10,
        "volume": 4
    },
    {
        "id": "2.29",
        "group": "group C",
        "price": 333,
        "volume": 15
    },
    {
        "id": "2.30",
        "group": "group C",
        "price": 62,
        "volume": 20
    },
    {
        "id": "2.31",
        "group": "group C",
        "price": 76,
        "volume": 9
    },
    {
        "id": "2.32",
        "group": "group C",
        "price": 378,
        "volume": 8
    },
    {
        "id": "2.33",
        "group": "group C",
        "price": 483,
        "volume": 12
    },
    {
        "id": "2.34",
        "group": "group C",
        "price": 56,
        "volume": 18
    },
    {
        "id": "2.35",
        "group": "group C",
        "price": 322,
        "volume": 4
    },
    {
        "id": "2.36",
        "group": "group C",
        "price": 2,
        "volume": 6
    },
    {
        "id": "2.37",
        "group": "group C",
        "price": 331,
        "volume": 20
    },
    {
        "id": "2.38",
        "group": "group C",
        "price": 286,
        "volume": 13
    },
    {
        "id": "2.39",
        "group": "group C",
        "price": 399,
        "volume": 9
    },
    {
        "id": "2.40",
        "group": "group C",
        "price": 313,
        "volume": 17
    },
    {
        "id": "2.41",
        "group": "group C",
        "price": 253,
        "volume": 15
    },
    {
        "id": "2.42",
        "group": "group C",
        "price": 28,
        "volume": 13
    },
    {
        "id": "2.43",
        "group": "group C",
        "price": 12,
        "volume": 20
    },
    {
        "id": "2.44",
        "group": "group C",
        "price": 205,
        "volume": 16
    },
    {
        "id": "2.45",
        "group": "group C",
        "price": 27,
        "volume": 7
    },
    {
        "id": "2.46",
        "group": "group C",
        "price": 79,
        "volume": 7
    },
    {
        "id": "2.47",
        "group": "group C",
        "price": 465,
        "volume": 19
    },
    {
        "id": "2.48",
        "group": "group C",
        "price": 480,
        "volume": 16
    },
    {
        "id": "2.49",
        "group": "group C",
        "price": 126,
        "volume": 12
    },
    {
        "id": "2.50",
        "group": "group C",
        "price": 239,
        "volume": 19
    },
    {
        "id": "2.51",
        "group": "group C",
        "price": 300,
        "volume": 11
    },
    {
        "id": "2.52",
        "group": "group C",
        "price": 491,
        "volume": 20
    },
    {
        "id": "2.53",
        "group": "group C",
        "price": 390,
        "volume": 17
    }
]


