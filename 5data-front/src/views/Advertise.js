import React from 'react'

import { Card, CardTitle, Col } from "reactstrap"
import CountUp from 'react-countup';

import { AiOutlineLineChart } from 'react-icons/ai'

import AxiosHelp from "../tools/AxiosHelp"

class Advertise extends React.Component {

    state = {
        studentsCount : 0
    }

    componentDidMount(){
        this.loadData()
    }

    render() {
        return <React.Fragment>

            <div className="text-center">
                <h1 >Introduction</h1>
                <p>Ce travail aura pour but de présenter et d'étudier les données<br />
                    des étudiants issus de notre jeu de données.<br />
                    <br />
                    Nous mettrons en avant des faits statistiques et apporterons<br />
                    des solutions aux problèmes soulevés <br />
                    <br />
                    Ce sujet repose sur une architecture de big data.<br />
                    Les technologies employées seront décrites dans les annexes.<br />
                </p>
                <Col md={{ size: 2, offset: 5 }} >
                    <Card body color="primary" className="text-black" className="mt-5">
                        <CardTitle tag="h5">
                            <AiOutlineLineChart size={50} /><br />
                            Quantité de données

                        </CardTitle>
                        <div className="counter">
                            <CountUp end={this.state.studentsCount} duration={3} separator=" " />
                        </div>
                    </Card>
                </Col>

            </div>


        </React.Fragment>
    }


    loadData() {
        AxiosHelp.axios().get("/api/data/studentsCount")
            .then(response => {
                this.setState({
                    studentsCount:response.data.quantiteDeDonnees
                })
            });
    }

} export default Advertise