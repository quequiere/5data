import React from "react"
import Advertise from "./Advertise"
import StudentsMap from "./StudentsMap"
import StudentsOrig from "./StudentsOrig"
import Grade from "./Grade"
import Travel from "./Travel"
import Recrutement from "./Recrutement"

class GlobalView extends React.Component{
    render(){ return <React.Fragment>
        <Advertise/>
        <StudentsMap/>
        <StudentsOrig/>
        <Grade/>
        <Travel/>
        <Recrutement/>
    </React.Fragment>}
}
export default GlobalView