import React from "react"

import { MdWork } from "react-icons/md"

import { Col, Card, CardTitle, Row } from "reactstrap"

import { ResponsiveBar } from '@nivo/bar'
import { ResponsiveLine } from '@nivo/line'

import AxiosHelp from "../tools/AxiosHelp"

class Recrutement extends React.Component {

    state = {
        top10: [],
        markSpeedRecruit:[],
        bestRecruiters : [],
    }

    componentDidMount(){
        this.loadData()
    }



    loadData() {

        AxiosHelp.axios().get("/api/data/bestRecruiters")
        .then(response => {

            let finalData = response.data.nbEtudiantsRecruteParRecruteurs.map(r => {
                return {
                    ecole : r.nomRecruteur,
                    mois: r.nbElevesRecrutes
                }
            })

            this.setState({
                bestRecruiters:finalData
            })
        });

        // ======================================================

        AxiosHelp.axios().get("/api/data/speedRecruitingByMark")
        .then(response => {

            let finalData =  [  {
                        "id": "",
                        "data": [
                            {
                                "x": "0 - 4",
                                "y": response.data.dureeRecrutementNoteZeroAQuatre
                            },
                            {
                                "x": "5 - 8",
                                "y": response.data.dureeRecrutementNoteCinqHuit
                            },
                            {
                                "x": "9 - 12",
                                "y": response.data.dureeRecrutementNoteNeufDouze
                            },
                            {
                                "x": "13 - 16",
                                "y": response.data.dureeRecrutementNoteTreizeSeize
                            },
                            {
                                "x": "17 - 20",
                                "y": response.data.dureeRecrutementNoteDixseptVingt
                            }
                        ]
                    }
                ]

            this.setState({
                markSpeedRecruit:finalData
            })
        });

        // =======================================================
        AxiosHelp.axios().get("/api/data/speedRecruitingBySChool")
            .then(response => {

                let finalData = response.data.dureeRecrutementParGroupeEcoles.map(r => {
                    return {
                        ecole : r.nomEcole,
                        mois: r.valeurPourCetteEcole
                    }
                })

                this.setState({
                    top10:finalData
                })
            });
    }


    render() {
        return <React.Fragment>
            <div className="text-center mt-5">
                <hr />
                <h1 className="mb-5">< MdWork size={100} /><br />Le recrutement</h1>
                <Col md={{ size: 4, offset: 4 }}>
                    <Card body color="success">
                        Nous allons ici faire l'état des lieux du recrutement
                </Card>
                </Col>

                <Row className="mt-5">
                    <Col md={6}>
                        <h2>Top 10 des écoles qui recrutent rapidement</h2>
                        <div className="chart whitechart" >
                            <ResponsiveBar
                                animate={false}
                                data={this.state.top10}
                                keys={['mois']}
                                indexBy="ecole"
                                margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
                                padding={0.3}
                                valueScale={{ type: 'linear' }}
                                indexScale={{ type: 'band', round: true }}
                                colors={{ scheme: 'nivo' }}
                                borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                                axisTop={null}
                                axisRight={null}
                                axisBottom={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: "Nom des écoles",
                                    legendPosition: 'middle',
                                    legendOffset: 32
                                }}
                                axisLeft={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'Nombre de mois avant recrutement',
                                    legendPosition: 'middle',
                                    legendOffset: -40
                                }}
                                labelSkipWidth={12}
                                labelSkipHeight={12}
                                labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                                legends={[
                                    {
                                        dataFrom: 'keys',
                                        anchor: 'bottom-right',
                                        direction: 'column',
                                        justify: false,
                                        translateX: 120,
                                        translateY: 0,
                                        itemsSpacing: 2,
                                        itemWidth: 100,
                                        itemHeight: 20,
                                        itemDirection: 'left-to-right',
                                        itemOpacity: 0.85,
                                        symbolSize: 20,
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemOpacity: 1
                                                }
                                            }
                                        ]
                                    }
                                ]}
                                animate={true}
                                motionStiffness={90}
                                motionDamping={15}
                            />
                        </div>
                        <Col md={{ size: 6, offset: 3 }}>
                            <Card body color="primary">
                                D'après nos statistiques il semble que supinfo soit l'école dont les élèves sont recrutés le plus rapidement.
                            </Card>
                        </Col>
                    </Col>
                    <Col md={6}>
                        <h2>Temps de recrutement en fonction des résultats</h2>
                        <div className="chart whitechart" >
                            <ResponsiveLine
                                animate={false}
                                data={this.state.markSpeedRecruit}
                                margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
                                xScale={{ type: 'point' }}
                                yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: false, reverse: false }}
                                yFormat=" >-.2f"
                                axisTop={null}
                                axisRight={null}
                                axisBottom={{
                                    orient: 'bottom',
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'Note à l examen',
                                    legendOffset: 36,
                                    legendPosition: 'middle'
                                }}
                                axisLeft={{
                                    orient: 'left',
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: "Mois avant recrutement",
                                    legendOffset: -40,
                                    legendPosition: 'middle'
                                }}
                                pointSize={10}
                                pointColor={{ theme: 'background' }}
                                pointBorderWidth={2}
                                pointBorderColor={{ from: 'serieColor' }}
                                pointLabelYOffset={-12}
                                useMesh={true}
                                legends={[
                                    {
                                        anchor: 'bottom-right',
                                        direction: 'column',
                                        justify: false,
                                        translateX: 100,
                                        translateY: 0,
                                        itemsSpacing: 0,
                                        itemDirection: 'left-to-right',
                                        itemWidth: 80,
                                        itemHeight: 20,
                                        itemOpacity: 0.75,
                                        symbolSize: 12,
                                        symbolShape: 'circle',
                                        symbolBorderColor: 'rgba(0, 0, 0, .5)',
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemBackground: 'rgba(0, 0, 0, .03)',
                                                    itemOpacity: 1
                                                }
                                            }
                                        ]
                                    }
                                ]}
                            />
                        </div>
                        <Col md={{ size: 6, offset: 3 }}>
                            <Card body color="primary">
                                Grâce à ce graphique nous pouvons voir que le délais de crutement moyen des élèves set de 4 mois.
                            </Card>
                        </Col>
                    </Col>
                </Row>





                <Row className="mt-5">
                    <Col md={12}>
                        <h2>Les meileures recruteurs d'élèves supinfo</h2>
                        <div className="chart whitechart" >
                            <ResponsiveBar
                                animate={false}
                                data={this.state.bestRecruiters}
                                keys={['mois']}
                                indexBy="ecole"
                                margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
                                padding={0.3}
                                valueScale={{ type: 'linear' }}
                                indexScale={{ type: 'band', round: true }}
                                colors={{ scheme: 'accent' }}
                                borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                                axisTop={null}
                                axisRight={null}
                                axisBottom={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: "Nom du recruteur",
                                    legendPosition: 'middle',
                                    legendOffset: 32
                                }}
                                axisLeft={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'Quantité recrutée',
                                    legendPosition: 'middle',
                                    legendOffset: -40
                                }}
                                labelSkipWidth={12}
                                labelSkipHeight={12}
                                labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                                legends={[
                                    {
                                        dataFrom: 'keys',
                                        anchor: 'bottom-right',
                                        direction: 'column',
                                        justify: false,
                                        translateX: 120,
                                        translateY: 0,
                                        itemsSpacing: 2,
                                        itemWidth: 100,
                                        itemHeight: 20,
                                        itemDirection: 'left-to-right',
                                        itemOpacity: 0.85,
                                        symbolSize: 20,
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemOpacity: 1
                                                }
                                            }
                                        ]
                                    }
                                ]}
                                animate={true}
                                motionStiffness={90}
                                motionDamping={15}
                            />
                        </div>
                        <Col md={{ size: 6, offset: 3 }}>
                            <Card body color="primary">
                                Pour finir, ce graphique nous présente les top recruteurs des élèves supinfo. En fonction de la génération les meilleurs recruteurs sont par exemple:
                                Amazon, Microsoft, Apple ou encore Tesla
                            </Card>
                        </Col>
                    </Col>
                </Row>
            </div>

        </React.Fragment>
    }

} export default Recrutement




