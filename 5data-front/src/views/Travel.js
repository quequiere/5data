import React from "react"

import { MdTrain } from "react-icons/md"

import { ResponsiveHeatMap } from '@nivo/heatmap'
import { ResponsiveBar } from '@nivo/bar'

import { Col, Card, CardTitle, Row } from "reactstrap"

import AxiosHelp from "../tools/AxiosHelp"

class Travel extends React.Component {


    state = {
        heatmapData: [],
        travelData: []
    }

    componentDidMount() {
        this.loadData()
    }


    loadData() {

        AxiosHelp.axios().get("/api/data/markByTravelTime")
            .then(response => {

                let finalData =
                    [
                        {
                            "traveltime": "<15",
                            "0 - 4": response.data.zeroAQuatreMoinsDeQuinzeMinutes,
                            "5 - 8": response.data.cinqAHuitMoinsDeQuinzeMinutes,
                            "9 - 12": response.data.neufADouzeMoinsDeQuinzeMinutes,
                            "13 - 16": response.data.treizeASeizeMoinsDeQuinzeMinutes,
                            "17 - 20": response.data.dixseptAVingtMoinsDeQuinzeMinutes,
                        },
                        {
                            "traveltime": "15-30",
                            "0 - 4": response.data.zeroAQuatreQuinzeATrenteMinutes,
                            "5 - 8": response.data.cinqAHuitQuinzeATrenteMinutes,
                            "9 - 12": response.data.neufADouzeQuinzeATrenteMinutes,
                            "13 - 16": response.data.treizeASeizeQuinzeATrenteMinutes,
                            "17 - 20": response.data.dixseptAVingtQuinzeATrenteMinutes,
                        },
                        {
                            "traveltime": "30-1h",
                            "0 - 4": response.data.zeroAQuatreTrenteASoixanteMinutes,
                            "5 - 8": response.data.cinqAHuitTrenteASoixanteMinutes,
                            "9 - 12": response.data.neufADouzeTrenteASoixanteMinutes,
                            "13 - 16": response.data.treizeASeizeTrenteASoixanteMinutes,
                            "17 - 20": response.data.dixseptAVingtTrenteASoixanteMinutes,
                        },
                        {
                            "traveltime": ">1h",
                            "0 - 4": response.data.zeroAQuatrePlusDeSoixanteMinutes,
                            "5 - 8": response.data.cinqAHuitPlusDeSoixanteMinutes,
                            "9 - 12": response.data.neufADouzePlusDeSoixanteMinutes,
                            "13 - 16": response.data.treizeASeizePlusDeSoixanteMinutes,
                            "17 - 20": response.data.dixseptAVingtPlusDeSoixanteMinutes,
                        }

                    ]

                console.log(finalData)


                this.setState({
                    travelData: finalData
                })
            });

        // =====================================================


        AxiosHelp.axios().get("/api/data/travelRegionalTime")
            .then(response => {

                let finalData = response.data.map(regiondata => {
                    return {
                        region: regiondata[0].regionName,
                        "< 15m": regiondata[0].quantity,
                        "15 - 30": regiondata[1].quantity,
                        "30 - 1h": regiondata[2].quantity,
                        "4h +": regiondata[3].quantity,
                    }
                })

                console.log(finalData)


                this.setState({
                    heatmapData: finalData
                })
            });
    }


    render() {
        return <React.Fragment>

            <div className="text-center mt-5">
                <hr />
                <h1 className="mb-5">< MdTrain size={100} /><br />Les étudiants en mouvement</h1>
                <Col md={{ size: 4, offset: 4 }}>
                    <Card body color="success">
                        Analysons à présent l'impacte du temps de trajet sur les élèves
                </Card>
                </Col>
                <Row className="mt-5">
                    <Col md={6}>
                        <h2>Temps de trajet par région</h2>
                        <div className="chart whitechart" >
                            <ResponsiveHeatMap
                                animated={false}
                                data={this.state.heatmapData}
                                keys={[
                                    '< 15m',
                                    '15 - 30',
                                    '30 - 1h',
                                    '4h +'
                                ]}
                                indexBy="region"
                                margin={{ top: 20, right: 0, bottom: 0, left: 0 }}
                                sizeVariation={0.20}
                                forceSquare={true}
                                axisRight={null}
                                axisBottom={null}
                                axisLeft={{
                                    orient: 'left',
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'region',
                                    legendPosition: 'middle',
                                    legendOffset: -120
                                }}
                                cellOpacity={1}
                                cellBorderColor={{ from: 'color', modifiers: [['darker', 0.4]] }}
                                labelTextColor={{ from: 'color', modifiers: [['darker', 1.8]] }}
                                defs={[
                                    {
                                        id: 'lines',
                                        type: 'patternLines',
                                        background: 'inherit',
                                        color: 'rgba(0, 0, 0, 0.1)',
                                        rotation: -45,
                                        lineWidth: 4,
                                        spacing: 7
                                    }
                                ]}
                                fill={[{ id: 'lines' }]}
                                animate={true}
                                motionConfig="wobbly"
                                motionStiffness={80}
                                motionDamping={9}
                                hoverTarget="cell"
                                cellHoverOthersOpacity={0.25}
                            />
                        </div>
                        <Col md={{ size: 6, offset: 3 }}>
                            <Card body color="primary">
                                D'après cette heatmap nous pouvons remarquer que les temps de trajet globaux se rapprochent généralement de 15 minutes. <br />
                                L'état de new york est plutôt bien désservi, proportionnellement la majeure partie de leurs étudiants ont un temps de trajet bas.
                            </Card>
                        </Col>
                    </Col>


                    <Col md={6}>
                        <h2>Notes en fonction du temps de trajet</h2>
                        <div className="chart whitechart" >
                            <ResponsiveBar
                                animate={false}
                                data={this.state.travelData}
                                keys={['0 - 4', '5 - 8', '9 - 12', '13 - 16', '17 - 20']}
                                indexBy="traveltime"
                                margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
                                padding={0.3}
                                valueScale={{ type: 'linear' }}
                                indexScale={{ type: 'band', round: true }}
                                colors={{ scheme: 'nivo' }}
                                borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                                axisTop={null}
                                axisRight={null}
                                axisBottom={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: "Temps de trajet",
                                    legendPosition: 'middle',
                                    legendOffset: 32
                                }}
                                axisLeft={{
                                    tickSize: 5,
                                    tickPadding: 5,
                                    tickRotation: 0,
                                    legend: 'quantité',
                                    legendPosition: 'middle',
                                    legendOffset: -40
                                }}
                                labelSkipWidth={12}
                                labelSkipHeight={12}
                                labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                                legends={[
                                    {
                                        dataFrom: 'keys',
                                        anchor: 'bottom-right',
                                        direction: 'column',
                                        justify: false,
                                        translateX: 120,
                                        translateY: 0,
                                        itemsSpacing: 2,
                                        itemWidth: 100,
                                        itemHeight: 20,
                                        itemDirection: 'left-to-right',
                                        itemOpacity: 0.85,
                                        symbolSize: 20,
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemOpacity: 1
                                                }
                                            }
                                        ]
                                    }
                                ]}
                                animate={true}
                                motionStiffness={90}
                                motionDamping={15}
                            />
                        </div>

                        <Col md={{ size: 6, offset: 3 }}>
                            <Card body color="primary">
                                Toutes proportions gardées, le temps de trajet ne sembre pas avoir une influence sur la note finale de l'élève
                            </Card>
                        </Col>

                    </Col>
                </Row>

            </div>



        </React.Fragment>
    }
}
export default Travel
