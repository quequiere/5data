import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "./components/Globalstyle";
import {darkTheme } from "./components/Themes"
import "./styles/customcss.css"

import HeaderPage from "./views/HeaderPage"
import GlobalView from "./views/GlobalView"

function App() {

  return (
    <ThemeProvider theme={darkTheme}>
        <GlobalStyles />
        <HeaderPage/>
        <GlobalView/>
        <div style={{minHeight:"100px"}}>

        </div>

      </ThemeProvider>
  );
}

export default App;
