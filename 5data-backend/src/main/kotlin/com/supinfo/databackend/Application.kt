package com.supinfo.databackend

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling
import springfox.documentation.swagger2.annotations.EnableSwagger2

@EnableScheduling
@EnableSwagger2
@SpringBootApplication(exclude = [CassandraAutoConfiguration::class])
class Application
fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
