package com.supinfo.databackend.provider

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.CacheManager
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class RedisProvider @Autowired constructor(val cacheManager: CacheManager){

    @PostConstruct
    fun tryCache(){
        println("======================")
        println("")
        println("")
        println("   Try redis cache")
        println("")
        println("")


        saveValueToCache("hello","world")
        val result = getValueFromCache("hello",String::class.java)

        removeValueFromCache("hello")
        val resultAfterDeleted = getValueFromCache("hello",String::class.java)

        println("Saved : $result   After delete: $resultAfterDeleted")



        println("")
        println("")
        println("======================")

    }


    fun <T> getValueFromCache(key: String,type : Class<T>): T? {
        val value =  cacheManager.getCache("5datacache")!!.get(key,type)
        return value
    }

    fun <T> getValueFromCacheNotNull(key: String,type : Class<T>): T {
        val value =  cacheManager.getCache("5datacache")!!.get(key,type)?:throw Exception("Data not found")
        return value
    }

    fun saveValueToCache(key: String, value: Any) {
        cacheManager.getCache("5datacache")!!.put(key,value)
    }


    fun removeValueFromCache(key: String) {
        cacheManager.getCache("5datacache")!!.evictIfPresent(key)
    }
}