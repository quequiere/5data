package com.supinfo.databackend.models

import java.io.Serializable


data class Recruteur(val nomRecruteur: String, val nbElevesRecrutes : Int) : Serializable{
    constructor() : this("", -1)
}
