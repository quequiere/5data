package com.supinfo.databackend.models

import java.io.Serializable


data class NosEtudiantsATraversLeMonde (val nbEtudiants: Int,
                                        val nbEcoles: Int,
                                        val nbRegions: Int) : Serializable{
    constructor() : this(-1,
            -1,
            -1)
}
