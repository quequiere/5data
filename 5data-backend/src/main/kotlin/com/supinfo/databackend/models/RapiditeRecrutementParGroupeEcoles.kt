package com.supinfo.databackend.models

import java.io.Serializable


data class RapiditeRecrutementParGroupeEcoles (val dureeRecrutementParGroupeEcoles: List<Ecole>?) : Serializable{
    constructor() : this(emptyList())
}
