package com.supinfo.databackend.models

import java.io.Serializable


data class ImpactSalonsEtudiants(val nombreScolarisationParAn: List<Scolarisation>) : Serializable{
    constructor() : this(emptyList())
}
