package com.supinfo.databackend.models

import java.io.Serializable


data class RepartitionDesNotesParRegions (val zeroAQuatreParRegion: List<Region>?,
                                          val cinqAHuitParRegion: List<Region>?,
                                          val neufADouzeParRegion: List<Region>?,
                                          val treizeASeizeParRegion: List<Region>?,
                                          val dixseptAVingtParRegion: List<Region>?) : Serializable{
    constructor() : this(emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),
            emptyList(),)
}
