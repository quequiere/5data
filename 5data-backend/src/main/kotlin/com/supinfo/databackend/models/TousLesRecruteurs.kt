package com.supinfo.databackend.models

import java.io.Serializable


data class TousLesRecruteurs (val tousLesRecruteurs: List<String>?) : Serializable{
    constructor() : this(emptyList())
}
