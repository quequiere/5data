package com.supinfo.databackend.models

import java.io.Serializable


data class MotifArretEtudes(val motifArretEtudes: String, val nbArretEtudes : Int) : Serializable{
    constructor() : this("", -1)
}
