package com.supinfo.databackend.models

import java.io.Serializable


data class MotifContratPro(val motifContratPro: String, val nbContratPro : Int) : Serializable{
    constructor() : this("", -1)
}
