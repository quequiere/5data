package com.supinfo.databackend.models

import java.io.Serializable


data class TousLesGroupesEcoles (val tousLesGroupesEcoles: List<String>?) : Serializable{
    constructor() : this(emptyList())
}
