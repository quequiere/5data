package com.supinfo.databackend.models

import java.io.Serializable


data class PredictionNbScolarisation (val nbScolarisationNmoinsUn: Int,
                                      val nbScolarisationN: Int) : Serializable{
    constructor() : this(-1,
            -1)

    fun prediction() : Int {
        return nbScolarisationN + (nbScolarisationN - nbScolarisationNmoinsUn)
    }

    fun predictionPercentage() : Int {
        return ((nbScolarisationN - nbScolarisationNmoinsUn)/nbScolarisationNmoinsUn)*100
    }
}
