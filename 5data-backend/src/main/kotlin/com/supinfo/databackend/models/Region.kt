package com.supinfo.databackend.models

import java.io.Serializable


data class Region(val nomRegion: String, val valeurPourCetteRegion : Int) : Serializable{
    constructor() : this("", -1)
}
