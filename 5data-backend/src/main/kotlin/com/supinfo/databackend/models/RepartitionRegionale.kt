package com.supinfo.databackend.models

import java.io.Serializable


data class RepartitionRegionale(val nbEtudiantsParRegion: List<Region>?) : Serializable{
    constructor() : this(emptyList())
}
