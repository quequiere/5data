package com.supinfo.databackend.models

import java.io.Serializable


data class NotesEnFonctionDuSexe (val zeroAQuatreFille: Int,
                                  val cinqAHuitFille: Int,
                                  val neufADouzeFille: Int,
                                  val treizeASeizeFille: Int,
                                  val dixseptAVingtFille: Int,
                                  val zeroAQuatreGarcon: Int,
                                  val cinqAHuitGarcon: Int,
                                  val neufADouzeGarcon: Int,
                                  val treizeASeizeGarcon: Int,
                                  val dixseptAVingtGarcon: Int) : Serializable{
    constructor() : this(-1,
            -1,
            -1,
            -1,
            -1,
            -1,
            -1,
            -1,
            -1,
            -1)
}
