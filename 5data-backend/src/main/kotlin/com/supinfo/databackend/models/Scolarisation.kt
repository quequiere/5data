package com.supinfo.databackend.models

import java.io.Serializable


data class Scolarisation(val anneeScolarisation: Int, val nombreEtudiantsArrives : Int) : Serializable{
    constructor() : this(-1, -1)
}
