package com.supinfo.databackend.models

import java.io.Serializable


data class TousLesMotifsArretsContratsPro (val tousLesMotifsArretsContratsPro: List<String>?) : Serializable{
    constructor() : this(emptyList())
}
