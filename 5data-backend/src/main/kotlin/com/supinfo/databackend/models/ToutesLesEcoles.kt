package com.supinfo.databackend.models

import java.io.Serializable


data class ToutesLesEcoles (val toutesLesEcoles: List<String>?) : Serializable{
    constructor() : this(emptyList())
}
