package com.supinfo.databackend.models

import java.io.Serializable


data class ContratsProParMotif(val nbContratsProParMotif: List<MotifContratPro>?) : Serializable{
    constructor() : this(emptyList())
}
