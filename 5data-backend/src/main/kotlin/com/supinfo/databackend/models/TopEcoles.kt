package com.supinfo.databackend.models

import java.io.Serializable


data class TopEcoles (val nbEtudiantsParEcole: List<Ecole>?) : Serializable{
    constructor() : this(emptyList())
}
