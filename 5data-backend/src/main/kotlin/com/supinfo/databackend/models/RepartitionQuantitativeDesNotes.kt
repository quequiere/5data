package com.supinfo.databackend.models

import java.io.Serializable


data class RepartitionQuantitativeDesNotes (val zeroAQuatre: Int,
                                            val cinqAHuit: Int,
                                            val neufADouze: Int,
                                            val treizeASeize: Int,
                                            val dixseptAVingt: Int) : Serializable{
    constructor() : this(-1,
            -1,
            -1,
            -1,
            -1)
}
