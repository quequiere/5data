package com.supinfo.databackend.models

import java.io.Serializable


data class ArretEtudes(val arretsEtudesParMotifs: List<MotifArretEtudes>?) : Serializable{
    constructor() : this(emptyList())
}
