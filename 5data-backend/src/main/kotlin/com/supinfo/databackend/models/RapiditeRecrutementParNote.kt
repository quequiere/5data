package com.supinfo.databackend.models

import java.io.Serializable


data class RapiditeRecrutementParNote (val dureeRecrutementNoteZeroAQuatre: Int,
                                       val dureeRecrutementNoteCinqHuit: Int,
                                       val dureeRecrutementNoteNeufDouze: Int,
                                       val dureeRecrutementNoteTreizeSeize: Int,
                                       val dureeRecrutementNoteDixseptVingt: Int) : Serializable{
    constructor() : this(-1,
            -1,
            -1,
            -1,
            -1,)
}
