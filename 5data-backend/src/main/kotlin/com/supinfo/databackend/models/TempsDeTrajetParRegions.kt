package com.supinfo.databackend.models

import java.io.Serializable


data class TempsDeTrajetParRegions (val nbEtudiantsParRegionMoinsDeQuinzeMinutes: List<Region>?,
                                    val nbEtudiantsParRegionQuinzeATrenteMinutes: List<Region>?,
                                    val nbEtudiantsParRegionTrenteASoixanteMinutes: List<Region>?,
                                    val nbEtudiantsParRegionPlusDeSoixanteMinutes: List<Region>?) : Serializable{
    constructor() : this(emptyList(),
            emptyList(),
            emptyList(),
            emptyList())
}
