package com.supinfo.databackend.models

import java.io.Serializable


data class Introduction (val quantiteDeDonnees: Int) : Serializable{
    constructor() : this(-1)
}
