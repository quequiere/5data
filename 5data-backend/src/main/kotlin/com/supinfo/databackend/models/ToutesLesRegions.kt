package com.supinfo.databackend.models

import java.io.Serializable


data class ToutesLesRegions (val toutesLesRegions: List<String>?) : Serializable{
    constructor() : this(emptyList())
}
