package com.supinfo.databackend.models

import java.io.Serializable


data class Ecole(val nomEcole: String, val valeurPourCetteEcole : Int) : Serializable{
    constructor() : this("", -1)
}
