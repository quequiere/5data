package com.supinfo.databackend.models

import java.io.Serializable


data class ContratsProParRegions(val nbContratsProParRegions: List<Region>?) : Serializable{
    constructor() : this(emptyList())
}
