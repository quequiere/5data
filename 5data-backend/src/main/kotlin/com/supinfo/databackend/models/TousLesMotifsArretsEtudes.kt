package com.supinfo.databackend.models

import java.io.Serializable


data class TousLesMotifsArretsEtudes (val tousLesMotifsArretsEtudes: List<String>?) : Serializable{
    constructor() : this(emptyList())
}
