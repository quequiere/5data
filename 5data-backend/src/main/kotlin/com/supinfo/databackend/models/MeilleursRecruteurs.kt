package com.supinfo.databackend.models

import java.io.Serializable


data class MeilleursRecruteurs (val nbEtudiantsRecruteParRecruteurs: List<Recruteur>?) : Serializable{
    constructor() : this(emptyList())
}
