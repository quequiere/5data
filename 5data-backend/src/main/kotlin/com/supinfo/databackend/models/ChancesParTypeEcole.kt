package com.supinfo.databackend.models

import java.io.Serializable


data class ChancesParTypeEcole (val successPrive: Int,
                                val failedPrive: Int,
                                val successPublic: Int,
                                val failedPublic: Int) : Serializable{
    constructor() : this(-1,
            -1,
            -1,
            -1)
}
