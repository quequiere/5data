package com.supinfo.databackend.services

import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector.cql.CassandraConnectorConf
import com.datastax.spark.connector.japi.CassandraJavaUtil
import com.supinfo.databackend.models.*
import com.supinfo.databackend.provider.RedisProvider
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


/**
 * We need to include jar connector to spark
 * See https://stackoverrun.com/fr/q/10765241 for some problems about incompatibility
 */

@Service
class DataComputeService @Autowired constructor(val sparkSession: SparkSession, val redisProvider : RedisProvider, val sparkConf : SparkConf) {

    @Scheduled(initialDelay = 15*1000, fixedDelay = 60*1000)
    fun computeDataWithSpark() {

        println(" ==============================  ")
        println("")
        println("")
        println("Stat work Started")
        println("")
        println("")
        println(" ==============================  ")

        // Methode 1
        val functions = CassandraJavaUtil.javaFunctions(sparkSession.sparkContext())
        val tableSchoolStudent = functions.cassandraTable("dataspace", "schoolstudent")

        // Methode 2 (plus rapide)
        val connector = CassandraConnector(CassandraConnectorConf.fromSparkConf(sparkConf))
        val sess = connector.openSession()

        // Nombre etudiants
        val nbTotalEtudiants = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent").first().getObject(0).toString())
        println(nbTotalEtudiants)
        redisProvider.saveValueToCache("nbTotalEtudiants", nbTotalEtudiants)

        // Graphique Introduction
        val introduction = Introduction(nbTotalEtudiants)
        println(introduction)
        redisProvider.saveValueToCache("introduction", introduction)

        // Ecoles
        var toutesLesEcolesCassandraRow = sess.execute("SELECT school_name FROM dataspace.schoolstudent").toList()
        var toutesLesEcolesAvecDoublons : List<String> = emptyList()
        toutesLesEcolesCassandraRow.forEach { element -> toutesLesEcolesAvecDoublons += element.getString(0).toString() }
        var ecoles = toutesLesEcolesAvecDoublons.distinct()
        println(ecoles)
        redisProvider.saveValueToCache("ecoles", ToutesLesEcoles(ecoles))

        // Regions
        var toutesLesRegionsCassandraRow = sess.execute("SELECT region FROM dataspace.schoolstudent").toList()//.getObject(0).toString()
        var toutesLesRegionsAvecDoublons : List<String> = emptyList()
        toutesLesRegionsCassandraRow.forEach { element -> toutesLesRegionsAvecDoublons += element.getString(0).toString() }
        var regions = toutesLesRegionsAvecDoublons.distinct()
        println(regions)
        redisProvider.saveValueToCache("regions", ToutesLesRegions(regions))

        // Graphique nos etudiants a travers le monde
        val nosEtudiantsATraversLeMonde = NosEtudiantsATraversLeMonde(nbTotalEtudiants,ecoles.count(),regions.count())
        println(nosEtudiantsATraversLeMonde)
        redisProvider.saveValueToCache("nosEtudiantsATraversLeMonde", nosEtudiantsATraversLeMonde)

        // Graphique repartition regionale
        var nbEtudiantsParRegions: List<Region> = emptyList()
        regions.forEach { region ->
            nbEtudiantsParRegions += Region(region, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE region = '$region'").first().getObject(0).toString()))
        }
        val repartitionRegionale = RepartitionRegionale(nbEtudiantsParRegions)
        println(repartitionRegionale)
        redisProvider.saveValueToCache("repartitionRegionale", repartitionRegionale)

        // Groupes d'ecoles
        var tousLesGroupesEcolesCassandraRow = sess.execute("SELECT school_organization FROM dataspace.schoolstudent").toList()
        var tousLesGroupesEcolesAvecDoublons : List<String> = emptyList()
        tousLesGroupesEcolesCassandraRow.forEach { element -> tousLesGroupesEcolesAvecDoublons += element.getString(0).toString() }
        var groupesEcoles = tousLesGroupesEcolesAvecDoublons.distinct()
        println(groupesEcoles)
        redisProvider.saveValueToCache("groupesEcoles", TousLesGroupesEcoles(groupesEcoles))

        // Graphique Top groupes d'ecoles
        var nbEtudiantsParGroupeEcoles: List<Ecole> = emptyList()
        groupesEcoles.forEach { groupeEcoles ->
            nbEtudiantsParGroupeEcoles += Ecole(groupeEcoles, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE school_organization = '$groupeEcoles'").first().getObject(0).toString()))
        }
        val topEcoles = TopEcoles(nbEtudiantsParGroupeEcoles)
        println(topEcoles)
        redisProvider.saveValueToCache("topEcoles", topEcoles)

        // Graphique repartition quantitative des notes
        var notesParTranches = Array<Int>(21) { i -> 0}
        for (i in 0..20) {
            notesParTranches[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i").first().getObject(0).toString())
        }
        val repartitionQuantitativeDesNotes = RepartitionQuantitativeDesNotes(
                notesParTranches[0]+notesParTranches[1]+notesParTranches[2]+notesParTranches[3]+notesParTranches[4],
                notesParTranches[5]+notesParTranches[6]+notesParTranches[7]+notesParTranches[8],
                notesParTranches[9]+notesParTranches[10]+notesParTranches[11]+notesParTranches[12],
                notesParTranches[13]+notesParTranches[14]+notesParTranches[15]+notesParTranches[16],
                notesParTranches[17]+notesParTranches[18]+notesParTranches[19]+notesParTranches[20])
        println(repartitionQuantitativeDesNotes)
        redisProvider.saveValueToCache("repartitionQuantitativeDesNotes", repartitionQuantitativeDesNotes)

        // Graphique repartition quantitative des notes en fonction du sexe
        var notesParTranchesFilles = Array<Int>(21) { i -> 0}
        for (i in 0..20) {
            notesParTranchesFilles[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i AND sex = 'F' ALLOW FILTERING").first().getObject(0).toString())
        }
        var notesParTranchesGarcons = Array<Int>(21) { i -> 0}
        for (i in 0..20) {
            notesParTranchesGarcons[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i AND sex = 'M' ALLOW FILTERING").first().getObject(0).toString())
        }
        val notesEnFonctionDuSexe = NotesEnFonctionDuSexe(
                notesParTranchesFilles[0]+notesParTranchesFilles[1]+notesParTranchesFilles[2]+notesParTranchesFilles[3]+notesParTranchesFilles[4],
                notesParTranchesFilles[5]+notesParTranchesFilles[6]+notesParTranchesFilles[7]+notesParTranchesFilles[8],
                notesParTranchesFilles[9]+notesParTranchesFilles[10]+notesParTranchesFilles[11]+notesParTranchesFilles[12],
                notesParTranchesFilles[13]+notesParTranchesFilles[14]+notesParTranchesFilles[15]+notesParTranchesFilles[16],
                notesParTranchesFilles[17]+notesParTranchesFilles[18]+notesParTranchesFilles[19]+notesParTranchesFilles[20],
                notesParTranchesGarcons[0]+notesParTranchesGarcons[1]+notesParTranchesGarcons[2]+notesParTranchesGarcons[3]+notesParTranchesGarcons[4],
                notesParTranchesGarcons[5]+notesParTranchesGarcons[6]+notesParTranchesGarcons[7]+notesParTranchesGarcons[8],
                notesParTranchesGarcons[9]+notesParTranchesGarcons[10]+notesParTranchesGarcons[11]+notesParTranchesGarcons[12],
                notesParTranchesGarcons[13]+notesParTranchesGarcons[14]+notesParTranchesGarcons[15]+notesParTranchesGarcons[16],
                notesParTranchesGarcons[17]+notesParTranchesGarcons[18]+notesParTranchesGarcons[19]+notesParTranchesGarcons[20])
        println(notesEnFonctionDuSexe)
        redisProvider.saveValueToCache("notesEnFonctionDuSexe", notesEnFonctionDuSexe)

        // Graphique repartition des notes par regions
        var zeroAQuatreParRegion: List<Region> = emptyList()
        var cinqAHuitParRegion: List<Region> = emptyList()
        var neufADouzeParRegion: List<Region> = emptyList()
        var treizeASeizeParRegion: List<Region> = emptyList()
        var dixseptAVingtParRegion: List<Region> = emptyList()
        var notesParTranchesParRegions = Array<Int>(21) { i -> 0}
        regions.forEach { region ->
            for (i in 0..20) {
                notesParTranchesParRegions[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i AND region = '$region' ALLOW FILTERING").first().getObject(0).toString())
            }
            zeroAQuatreParRegion += Region(region, notesParTranchesParRegions[0]+notesParTranchesParRegions[1]+notesParTranchesParRegions[2]+notesParTranchesParRegions[3]+notesParTranchesParRegions[4])
            cinqAHuitParRegion += Region(region, notesParTranchesParRegions[5]+notesParTranchesParRegions[6]+notesParTranchesParRegions[7]+notesParTranchesParRegions[8])
            neufADouzeParRegion += Region(region, notesParTranchesParRegions[9]+notesParTranchesParRegions[10]+notesParTranchesParRegions[11]+notesParTranchesParRegions[12])
            treizeASeizeParRegion += Region(region, notesParTranchesParRegions[13]+notesParTranchesParRegions[14]+notesParTranchesParRegions[15]+notesParTranchesParRegions[16])
            dixseptAVingtParRegion += Region(region, notesParTranchesParRegions[17]+notesParTranchesParRegions[18]+notesParTranchesParRegions[19]+notesParTranchesParRegions[20])
        }
        val repartitionDesNotesParRegions = RepartitionDesNotesParRegions(zeroAQuatreParRegion, cinqAHuitParRegion, neufADouzeParRegion, treizeASeizeParRegion, dixseptAVingtParRegion)
        println(repartitionDesNotesParRegions)
        redisProvider.saveValueToCache("repartitionDesNotesParRegions", repartitionDesNotesParRegions)

        // Evaluation des chances par types d'ecoles
        val succesEcolesPubliques = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE public_or_independant = 'BC PUBLIC SCHOOL' AND failures = 0 ALLOW FILTERING").first().getObject(0).toString())
        val succesEcolesPrivees = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE public_or_independant = 'BC INDEPENDENT SCHOOL' AND failures = 0 ALLOW FILTERING").first().getObject(0).toString())
        val echecEcolesPubliques = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE public_or_independant = 'BC PUBLIC SCHOOL' AND failures > 0 ALLOW FILTERING").first().getObject(0).toString())
        val echecEcolesPrivees = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE public_or_independant = 'BC INDEPENDENT SCHOOL' AND failures > 0 ALLOW FILTERING").first().getObject(0).toString())
        val chancesParTypeEcole = ChancesParTypeEcole(succesEcolesPrivees,echecEcolesPrivees,succesEcolesPubliques,echecEcolesPubliques)
        println(chancesParTypeEcole)
        redisProvider.saveValueToCache("chancesParTypeEcole", chancesParTypeEcole)

        // Graphique temps de trajet par region
        var nbEtudiantsParRegionMoinsDeQuinzeMinutes: List<Region> = emptyList()
        var nbEtudiantsParRegionQuinzeATrenteMinutes: List<Region> = emptyList()
        var nbEtudiantsParRegionTrenteASoixanteMinutes: List<Region> = emptyList()
        var nbEtudiantsParRegionPlusDeSoixanteMinutes: List<Region> = emptyList()
        regions.forEach { region ->
            nbEtudiantsParRegionMoinsDeQuinzeMinutes += Region(region, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE region = '$region' AND traveltime  = 1 ALLOW FILTERING").first().getObject(0).toString()))
            nbEtudiantsParRegionQuinzeATrenteMinutes += Region(region, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE region = '$region' AND traveltime  = 2 ALLOW FILTERING").first().getObject(0).toString()))
            nbEtudiantsParRegionTrenteASoixanteMinutes += Region(region, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE region = '$region' AND traveltime  = 3 ALLOW FILTERING").first().getObject(0).toString()))
            nbEtudiantsParRegionPlusDeSoixanteMinutes += Region(region, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE region = '$region' AND traveltime  = 4 ALLOW FILTERING").first().getObject(0).toString()))
        }
        val tempsDeTrajetParRegions = TempsDeTrajetParRegions(nbEtudiantsParRegionMoinsDeQuinzeMinutes,nbEtudiantsParRegionQuinzeATrenteMinutes,nbEtudiantsParRegionTrenteASoixanteMinutes,nbEtudiantsParRegionPlusDeSoixanteMinutes)
        println(tempsDeTrajetParRegions)
        redisProvider.saveValueToCache("tempsDeTrajetParRegions", tempsDeTrajetParRegions)

        // Graphique notes en fonction du temps de trajet
        var notesParTranchesMoinsDeQuinze = Array<Int>(21) { i -> 0}
        var notesParTranchesQuinzeATrente = Array<Int>(21) { i -> 0}
        var notesParTranchesTrenteASoixante = Array<Int>(21) { i -> 0}
        var notesParTranchesPlusDeSoixante = Array<Int>(21) { i -> 0}
        for (i in 0..20) {
            notesParTranchesMoinsDeQuinze[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i AND traveltime = 1 ALLOW FILTERING").first().getObject(0).toString())
            notesParTranchesQuinzeATrente[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i AND traveltime = 2 ALLOW FILTERING").first().getObject(0).toString())
            notesParTranchesTrenteASoixante[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i AND traveltime = 3 ALLOW FILTERING").first().getObject(0).toString())
            notesParTranchesPlusDeSoixante[i] = Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE g1 = $i AND traveltime = 4 ALLOW FILTERING").first().getObject(0).toString())
        }
        var zeroAQuatreMoinsDeQuinzeMinutes = notesParTranchesMoinsDeQuinze[0]+notesParTranchesMoinsDeQuinze[1]+notesParTranchesMoinsDeQuinze[2]+notesParTranchesMoinsDeQuinze[3]+notesParTranchesMoinsDeQuinze[4]
        var cinqAHuitMoinsDeQuinzeMinutes = notesParTranchesMoinsDeQuinze[5]+notesParTranchesMoinsDeQuinze[6]+notesParTranchesMoinsDeQuinze[7]+notesParTranchesMoinsDeQuinze[8]
        var neufADouzeMoinsDeQuinzeMinutes = notesParTranchesMoinsDeQuinze[9]+notesParTranchesMoinsDeQuinze[10]+notesParTranchesMoinsDeQuinze[11]+notesParTranchesMoinsDeQuinze[12]
        var treizeASeizeMoinsDeQuinzeMinutes = notesParTranchesMoinsDeQuinze[13]+notesParTranchesMoinsDeQuinze[14]+notesParTranchesMoinsDeQuinze[15]+notesParTranchesMoinsDeQuinze[16]
        var dixseptAVingtMoinsDeQuinzeMinutes = notesParTranchesMoinsDeQuinze[17]+notesParTranchesMoinsDeQuinze[18]+notesParTranchesMoinsDeQuinze[19]+notesParTranchesMoinsDeQuinze[20]
        var zeroAQuatreQuinzeATrenteMinutes = notesParTranchesQuinzeATrente[0]+notesParTranchesQuinzeATrente[1]+notesParTranchesQuinzeATrente[2]+notesParTranchesQuinzeATrente[3]+notesParTranchesQuinzeATrente[4]
        var cinqAHuitQuinzeATrenteMinutes = notesParTranchesQuinzeATrente[5]+notesParTranchesQuinzeATrente[6]+notesParTranchesQuinzeATrente[7]+notesParTranchesQuinzeATrente[8]
        var neufADouzeQuinzeATrenteMinutes = notesParTranchesQuinzeATrente[9]+notesParTranchesQuinzeATrente[10]+notesParTranchesQuinzeATrente[11]+notesParTranchesQuinzeATrente[12]
        var treizeASeizeQuinzeATrenteMinutes = notesParTranchesQuinzeATrente[13]+notesParTranchesQuinzeATrente[14]+notesParTranchesQuinzeATrente[15]+notesParTranchesQuinzeATrente[16]
        var dixseptAVingtQuinzeATrenteMinutes = notesParTranchesQuinzeATrente[17]+notesParTranchesQuinzeATrente[18]+notesParTranchesQuinzeATrente[19]+notesParTranchesQuinzeATrente[20]
        var zeroAQuatreTrenteASoixanteMinutes = notesParTranchesTrenteASoixante[0]+notesParTranchesTrenteASoixante[1]+notesParTranchesTrenteASoixante[2]+notesParTranchesTrenteASoixante[3]+notesParTranchesTrenteASoixante[4]
        var cinqAHuitTrenteASoixanteMinutes = notesParTranchesTrenteASoixante[5]+notesParTranchesTrenteASoixante[6]+notesParTranchesTrenteASoixante[7]+notesParTranchesTrenteASoixante[8]
        var neufADouzeTrenteASoixanteMinutes = notesParTranchesTrenteASoixante[9]+notesParTranchesTrenteASoixante[10]+notesParTranchesTrenteASoixante[11]+notesParTranchesTrenteASoixante[12]
        var treizeASeizeTrenteASoixanteMinutes = notesParTranchesTrenteASoixante[13]+notesParTranchesTrenteASoixante[14]+notesParTranchesTrenteASoixante[15]+notesParTranchesTrenteASoixante[16]
        var dixseptAVingtTrenteASoixanteMinutes = notesParTranchesTrenteASoixante[17]+notesParTranchesTrenteASoixante[18]+notesParTranchesTrenteASoixante[19]+notesParTranchesTrenteASoixante[20]
        var zeroAQuatrePlusDeSoixanteMinutes = notesParTranchesPlusDeSoixante[0]+notesParTranchesPlusDeSoixante[1]+notesParTranchesPlusDeSoixante[2]+notesParTranchesPlusDeSoixante[3]+notesParTranchesPlusDeSoixante[4]
        var cinqAHuitPlusDeSoixanteMinutes = notesParTranchesPlusDeSoixante[5]+notesParTranchesPlusDeSoixante[6]+notesParTranchesPlusDeSoixante[7]+notesParTranchesPlusDeSoixante[8]
        var neufADouzePlusDeSoixanteMinutes = notesParTranchesPlusDeSoixante[9]+notesParTranchesPlusDeSoixante[10]+notesParTranchesPlusDeSoixante[11]+notesParTranchesPlusDeSoixante[12]
        var treizeASeizePlusDeSoixanteMinutes = notesParTranchesPlusDeSoixante[13]+notesParTranchesPlusDeSoixante[14]+notesParTranchesPlusDeSoixante[15]+notesParTranchesPlusDeSoixante[16]
        var dixseptAVingtPlusDeSoixanteMinutes = notesParTranchesPlusDeSoixante[17]+notesParTranchesPlusDeSoixante[18]+notesParTranchesPlusDeSoixante[19]+notesParTranchesPlusDeSoixante[20]
        val notesEnFonctionDuTempsDeTrajet = NotesEnFonctionDuTempsDeTrajet(
                zeroAQuatreMoinsDeQuinzeMinutes,
                cinqAHuitMoinsDeQuinzeMinutes,
                neufADouzeMoinsDeQuinzeMinutes,
                treizeASeizeMoinsDeQuinzeMinutes,
                dixseptAVingtMoinsDeQuinzeMinutes,
                zeroAQuatreQuinzeATrenteMinutes,
                cinqAHuitQuinzeATrenteMinutes,
                neufADouzeQuinzeATrenteMinutes,
                treizeASeizeQuinzeATrenteMinutes,
                dixseptAVingtQuinzeATrenteMinutes,
                zeroAQuatreTrenteASoixanteMinutes,
                cinqAHuitTrenteASoixanteMinutes,
                neufADouzeTrenteASoixanteMinutes,
                treizeASeizeTrenteASoixanteMinutes,
                dixseptAVingtTrenteASoixanteMinutes,
                zeroAQuatrePlusDeSoixanteMinutes,
                cinqAHuitPlusDeSoixanteMinutes,
                neufADouzePlusDeSoixanteMinutes,
                treizeASeizePlusDeSoixanteMinutes,
                dixseptAVingtPlusDeSoixanteMinutes
        )
        println(notesEnFonctionDuTempsDeTrajet)
        redisProvider.saveValueToCache("notesEnFonctionDuTempsDeTrajet", notesEnFonctionDuTempsDeTrajet)

        // Graphique recrutement rapides par groupes d'ecoles
        var nbMoisAvantRecrutementsParGroupeEcoles: List<Ecole> = emptyList()
        groupesEcoles.forEach { groupeEcoles ->
            nbMoisAvantRecrutementsParGroupeEcoles += Ecole(groupeEcoles, sess.execute("SELECT avg(nbmonthstobehired) FROM dataspace.schoolstudent WHERE school_organization = '$groupeEcoles'").first().getInt(0))
        }
        val rapiditeRecrutementParGroupeEcoles = RapiditeRecrutementParGroupeEcoles(nbMoisAvantRecrutementsParGroupeEcoles)
        println(rapiditeRecrutementParGroupeEcoles)
        redisProvider.saveValueToCache("rapiditeRecrutementParGroupeEcoles", rapiditeRecrutementParGroupeEcoles)

        // Graphique temps de recrutement en fonction des resultats
        var tempsRecrutementParNotesParTranches = Array<Int>(21) { i -> 0}
        for (i in 0..20) {
            tempsRecrutementParNotesParTranches[i] = sess.execute("SELECT avg(nbmonthstobehired) FROM dataspace.schoolstudent WHERE g1 = $i").first().getInt(0)
        }
        val rapiditeRecrutementParNote = RapiditeRecrutementParNote(
                (tempsRecrutementParNotesParTranches[0]+tempsRecrutementParNotesParTranches[1]+tempsRecrutementParNotesParTranches[2]+tempsRecrutementParNotesParTranches[3]+tempsRecrutementParNotesParTranches[4])/5,
                (tempsRecrutementParNotesParTranches[5]+tempsRecrutementParNotesParTranches[6]+tempsRecrutementParNotesParTranches[7]+tempsRecrutementParNotesParTranches[8])/4,
                (tempsRecrutementParNotesParTranches[9]+tempsRecrutementParNotesParTranches[10]+tempsRecrutementParNotesParTranches[11]+tempsRecrutementParNotesParTranches[12])/4,
                (tempsRecrutementParNotesParTranches[13]+tempsRecrutementParNotesParTranches[14]+tempsRecrutementParNotesParTranches[15]+tempsRecrutementParNotesParTranches[16])/4,
                (tempsRecrutementParNotesParTranches[17]+tempsRecrutementParNotesParTranches[18]+tempsRecrutementParNotesParTranches[19]+tempsRecrutementParNotesParTranches[20])/4)
        println(rapiditeRecrutementParNote)
        redisProvider.saveValueToCache("rapiditeRecrutementParNote", rapiditeRecrutementParNote)

        // Recruteurs
        var tousLesRecruteursCassandraRow = sess.execute("SELECT company FROM dataspace.schoolstudent").toList()
        var tousLesRecruteursAvecDoublons : List<String> = emptyList()
        tousLesRecruteursCassandraRow.forEach { element -> tousLesRecruteursAvecDoublons += element.getString(0).toString() }
        var recruteurs = tousLesRecruteursAvecDoublons.distinct()
        println(recruteurs)
        redisProvider.saveValueToCache("recruteurs", TousLesRecruteurs(recruteurs))

        // Graphique meilleurs recruteurs
        var nbEtudiantsParRecruteurs: List<Recruteur> = emptyList()
        recruteurs.forEach { recruteur ->
            nbEtudiantsParRecruteurs += Recruteur(recruteur, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE company = '$recruteur'").first().getObject(0).toString()))
        }
        val meilleursRecruteurs = MeilleursRecruteurs(nbEtudiantsParRecruteurs)
        println(meilleursRecruteurs)
        redisProvider.saveValueToCache("meilleursRecruteurs", meilleursRecruteurs)

        // NOUVEAU Graphique prediction nombre de scolarisation SUPINFO pour l'annee suivante (2018)
        val nbScolarisaitonSUPINFO2016 =  Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE start_year = 2016 AND school_organization = 'SUPINFO' ALLOW FILTERING").first().getObject(0).toString())
        val nbScolarisaitonSUPINFO2017 =  Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE start_year = 2017 AND school_organization = 'SUPINFO' ALLOW FILTERING").first().getObject(0).toString())
        val predictionNbScolarisation = PredictionNbScolarisation(nbScolarisaitonSUPINFO2016, nbScolarisaitonSUPINFO2017)
        println(predictionNbScolarisation)
        redisProvider.saveValueToCache("predictionNbScolarisation", predictionNbScolarisation)

        // Motifs arrets etudes
        var tousLesMotifsArretsEtudesCassandraRow = sess.execute("SELECT stop_studies_reason FROM dataspace.schoolstudent WHERE stop_studies = True").toList()
        var tousLesMotifsArretsEtudesAvecDoublons : List<String> = emptyList()
        tousLesMotifsArretsEtudesCassandraRow.forEach { element -> tousLesMotifsArretsEtudesAvecDoublons += element.getString(0).toString() }
        var motifsArretsEtudes = tousLesMotifsArretsEtudesAvecDoublons.distinct()
        println(motifsArretsEtudes)
        redisProvider.saveValueToCache("motifsArretsEtudes", TousLesMotifsArretsEtudes(motifsArretsEtudes))

        // NOUVEAU Graphique nombre arret etudes par motif
        var nbArretEtudesParMotif: List<MotifArretEtudes> = emptyList()
        motifsArretsEtudes.forEach { motifArretsEtudes ->
            nbArretEtudesParMotif += MotifArretEtudes(motifArretsEtudes, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE stop_studies_reason = '$motifArretsEtudes'").first().getObject(0).toString()))
        }
        val arretEtudes = ArretEtudes(nbArretEtudesParMotif)
        println(arretEtudes)
        redisProvider.saveValueToCache("arretEtudes", arretEtudes)

        // Motifs contrats pro
        var tousLesMotifsContratsProCassandraRow = sess.execute("SELECT pro_contract_reason FROM dataspace.schoolstudent WHERE pro_contract = True").toList()
        var tousLesMotifsContratsProAvecDoublons : List<String> = emptyList()
        tousLesMotifsContratsProCassandraRow.forEach { element -> tousLesMotifsContratsProAvecDoublons += element.getString(0).toString() }
        var motifsContratsPro = tousLesMotifsContratsProAvecDoublons.distinct()
        println(motifsContratsPro)
        redisProvider.saveValueToCache("motifsContratsPro", TousLesMotifsArretsContratsPro(motifsContratsPro))

        // NOUVEAU Graphique nombre contrats pro par motif
        var nbContratsProParMotif: List<MotifContratPro> = emptyList()
        motifsContratsPro.forEach { motifContratsPro ->
            nbContratsProParMotif += MotifContratPro(motifContratsPro, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE pro_contract_reason = '$motifContratsPro'").first().getObject(0).toString()))
        }
        val contratsProParMotif = ContratsProParMotif(nbContratsProParMotif)
        println(contratsProParMotif)
        redisProvider.saveValueToCache("contratsProParMotif", contratsProParMotif)

        // NOUVEAU Graphique nombre contrats pro par region
        var nbContratsProParRegions: List<Region> = emptyList()
        regions.forEach { region ->
            nbContratsProParRegions += Region(region, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE region = '$region' AND pro_contract = True ALLOW FILTERING").first().getObject(0).toString()))
        }
        val contratsProParRegions = ContratsProParRegions(nbContratsProParRegions)
        println(contratsProParRegions)
        redisProvider.saveValueToCache("contratsProParRegions", contratsProParRegions)

        // NOUVEAU Graphique impact salons etudiants sur nombre de scolarisations
        val nbScolarisaitonSansSalonsSUPINFO2015 = Scolarisation(2015, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE start_year = 2015 AND school_organization = 'SUPINFO' ALLOW FILTERING").first().getObject(0).toString()))
        val nbScolarisaitonSansSalonsSUPINFO2016 = Scolarisation(2016, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE start_year = 2016 AND school_organization = 'SUPINFO' ALLOW FILTERING").first().getObject(0).toString()))
        val nbScolarisaitonAvecSalonsSUPINFO2017 = Scolarisation(2017, Integer.parseInt(sess.execute("SELECT count(id) FROM dataspace.schoolstudent WHERE start_year = 2017 AND school_organization = 'SUPINFO' ALLOW FILTERING").first().getObject(0).toString()))
        val listeScolarisations: List<Scolarisation> = listOf(nbScolarisaitonSansSalonsSUPINFO2015,nbScolarisaitonSansSalonsSUPINFO2016,nbScolarisaitonAvecSalonsSUPINFO2017)
        val impactSalonsEtudiants = ImpactSalonsEtudiants(listeScolarisations)
        println(impactSalonsEtudiants)
        redisProvider.saveValueToCache("impactSalonsEtudiants",impactSalonsEtudiants)

       println(" ==============================  ")
        println("")
        println("")
        println("Stat work finished")
        println("")
        println("")
       println(" ==============================  ")
    }
}