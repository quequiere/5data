package com.supinfo.databackend.controller

import com.supinfo.databackend.models.*
import com.supinfo.databackend.models.common.DataResult
import com.supinfo.databackend.provider.RedisProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("data/")
@CrossOrigin(origins = ["*"])
class DataController @Autowired constructor(val redisProvider: RedisProvider) {

    @GetMapping("studentsCount")
    fun getStudentsCount(): Introduction {
        return redisProvider.getValueFromCacheNotNull("introduction", Introduction::class.java)
    }

    @GetMapping("studentsArroundWorld")
    fun worldStudents(): NosEtudiantsATraversLeMonde {
        return redisProvider.getValueFromCacheNotNull("nosEtudiantsATraversLeMonde", NosEtudiantsATraversLeMonde::class.java)
    }

    @GetMapping("regionRepartition")
    fun getRegonRepartion(): RepartitionRegionale {
        return redisProvider.getValueFromCacheNotNull("repartitionRegionale", RepartitionRegionale::class.java)
    }

    @GetMapping("topSchool")
    fun getTopSchools(): TopEcoles {
        return redisProvider.getValueFromCacheNotNull("topEcoles", TopEcoles::class.java)
    }

    @GetMapping("markQuantity")
    fun getMarkQuantity(): RepartitionQuantitativeDesNotes {
        return redisProvider.getValueFromCacheNotNull("repartitionQuantitativeDesNotes", RepartitionQuantitativeDesNotes::class.java)
    }

    @GetMapping("markbySex")
    fun getMarkBySex(): NotesEnFonctionDuSexe {
        return redisProvider.getValueFromCacheNotNull("notesEnFonctionDuSexe", NotesEnFonctionDuSexe::class.java)
    }

    @GetMapping("markByRegion")
    fun getMarkByRegion(): List<MarkByRegion> {
        val result = redisProvider.getValueFromCacheNotNull("repartitionDesNotesParRegions", RepartitionDesNotesParRegions::class.java)
        val ret: MutableList<MarkByRegion> = mutableListOf()

        ret.addAll(result.zeroAQuatreParRegion!!.map { MarkByRegion(it.nomRegion, it.valeurPourCetteRegion, "G1") })
        ret.addAll(result.cinqAHuitParRegion!!.map { MarkByRegion(it.nomRegion, it.valeurPourCetteRegion, "G2") })
        ret.addAll(result.neufADouzeParRegion!!.map { MarkByRegion(it.nomRegion, it.valeurPourCetteRegion, "G3") })
        ret.addAll(result.treizeASeizeParRegion!!.map { MarkByRegion(it.nomRegion, it.valeurPourCetteRegion, "G4") })
        ret.addAll(result.dixseptAVingtParRegion!!.map { MarkByRegion(it.nomRegion, it.valeurPourCetteRegion, "G5") })

        return ret.mapIndexed { index, markByRegion -> markByRegion.apply { this.id=index } }
    }

    @GetMapping("chanbeBySchoolType")
    fun getChanceBySchoolType(): ChancesParTypeEcole {
        return redisProvider.getValueFromCacheNotNull("chancesParTypeEcole", ChancesParTypeEcole::class.java)
    }


    @GetMapping("travelRegionalTime")
    fun getTravelRegionalTime(): Collection<List<TravelTimeRegion>> {
        val result =  redisProvider.getValueFromCacheNotNull("tempsDeTrajetParRegions", TempsDeTrajetParRegions::class.java)

        val allData = mutableListOf<TravelTimeRegion>()
        allData.addAll(result.nbEtudiantsParRegionMoinsDeQuinzeMinutes!!.map { TravelTimeRegion(it.nomRegion,it.valeurPourCetteRegion,"<15m") })
        allData.addAll(result.nbEtudiantsParRegionQuinzeATrenteMinutes!!.map { TravelTimeRegion(it.nomRegion,it.valeurPourCetteRegion,"15-30") })
        allData.addAll(result.nbEtudiantsParRegionTrenteASoixanteMinutes!!.map { TravelTimeRegion(it.nomRegion,it.valeurPourCetteRegion,"30-60") })
        allData.addAll(result.nbEtudiantsParRegionPlusDeSoixanteMinutes!!.map { TravelTimeRegion(it.nomRegion,it.valeurPourCetteRegion,">1h") })

        return allData.groupBy { it.regionName }.values

    }

    @GetMapping("markByTravelTime")
    fun getmarkByTravelTime(): NotesEnFonctionDuTempsDeTrajet {
        return redisProvider.getValueFromCacheNotNull("notesEnFonctionDuTempsDeTrajet", NotesEnFonctionDuTempsDeTrajet::class.java)
    }

    @GetMapping("speedRecruitingBySChool")
    fun getSpeedRecruitingBySChool(): RapiditeRecrutementParGroupeEcoles {
        return redisProvider.getValueFromCacheNotNull("rapiditeRecrutementParGroupeEcoles", RapiditeRecrutementParGroupeEcoles::class.java)
    }


    @GetMapping("speedRecruitingByMark")
    fun getspeedRecruitingByMark(): RapiditeRecrutementParNote {
        return redisProvider.getValueFromCacheNotNull("rapiditeRecrutementParNote", RapiditeRecrutementParNote::class.java)
    }


    @GetMapping("bestRecruiters")
    fun getbestRecruiters(): MeilleursRecruteurs {
        return redisProvider.getValueFromCacheNotNull("meilleursRecruteurs", MeilleursRecruteurs::class.java)
    }

}


data class TravelTimeRegion(val regionName: String, val quantity: Int, val travelTime : String)

data class MarkByRegion(val regionName: String, val quantity: Int, val mark: String,var id : Int = 0)