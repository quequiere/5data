package com.supinfo.databackend.conf

import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.sql.SparkSession
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer


/**
 * For debug on windows mount spark with
    spark-class org.apache.spark.deploy.master.Master --host 192.168.1.122
    spark-class org.apache.spark.deploy.worker.Worker spark://192.168.1.122:7077

    Install guide: https://phoenixnap.com/kb/install-spark-on-windows-10
    Dont forget to set en var hadoop home or spark will crash

    Version used 2.4.7
 */

@Configuration
class SparkConfig {

    @Value("\${proj.spark.HOST}")
    lateinit var sparkhost : String

    @Value("\${proj.cassandra.HOST}")
    lateinit var cassandraHost : String

    @Bean
    fun sparkConf(): SparkConf {
        return SparkConf()
                .setAppName("myappname")
                .setMaster("spark://${sparkhost}")
                .set("spark.cassandra.connection.host", cassandraHost)
                .set("spark.cassandra.auth.username", "cassandra")
                .set("spark.cassandra.auth.password", "password123")
    }

    @Bean
    fun javaSparkContext(sparkConf : SparkConf): JavaSparkContext {
        return JavaSparkContext(sparkConf).apply {
            this.addJar("spark-cassandra-connector_2.11-2.5.1.jar")
            this.addJar("java-driver-core-4.7.2.jar")
            this.addJar("spark-cassandra-connector-assembly_2.11-2.5.1.jar")
            this.addJar("config-1.3.4.jar")
        }
    }

    @Bean
    fun sparkSession(javaSparkContext : JavaSparkContext): SparkSession {
        return SparkSession
                .builder()
                .sparkContext(javaSparkContext.sc())
                .appName("Java Spark SQL basic example")
                .getOrCreate()
    }
    
}