package com.supinfo.databackend.conf

import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.web.cors.CorsConfiguration
import kotlin.jvm.Throws

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
                .csrf().disable()
                .cors().configurationSource {
                    CorsConfiguration()
                            .apply {
                                addAllowedMethod("*")
                                addAllowedOrigin("*")
                                addAllowedHeader("*")
                                addAllowedOriginPattern("*")
                            }

                }.and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "**").permitAll()
    }
}