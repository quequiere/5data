package com.supinfo.databackend.conf

import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.CacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.cache.RedisCacheConfiguration
import org.springframework.data.redis.cache.RedisCacheManager
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.connection.RedisStandaloneConfiguration
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.core.RedisTemplate

@Configuration
class CacheConfig {

    @Value("\${proj.redis.HOST}")
    lateinit var redisHost : String

    @Value("\${proj.spark.HOST}")
    lateinit var sparkhost : String

    @Bean
    fun redisConnectionFactory(): LettuceConnectionFactory {
        println("Redis (/Lettuce) enabled")
        val redisStandaloneConfiguration = RedisStandaloneConfiguration().apply {
            this.hostName = redisHost
            this.port = 6379
        }

        return LettuceConnectionFactory(redisStandaloneConfiguration)
    }

    @Bean
    fun redisTemplate(cf: RedisConnectionFactory) =
            RedisTemplate<String, String>().apply {
                this.setConnectionFactory(cf)
            }


    @Bean
    fun cacheConfiguration(): RedisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()


    @Bean
    fun cacheManager(redisConnectionFactory: RedisConnectionFactory): CacheManager {

        val cacheListTTL = listOf("5datacache").map { it to cacheConfiguration() }.toMap()

        return RedisCacheManager
                .builder(redisConnectionFactory)
                .cacheDefaults(cacheConfiguration())
                .withInitialCacheConfigurations( cacheListTTL)
                .build()
    }

}