import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.4.1"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"


	kotlin("jvm") version "1.4.21"
	kotlin("plugin.spring") version "1.4.21"
}


group = "com.supinfo"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
	mavenCentral()
}


dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")


	implementation("com.squareup.okhttp3:okhttp-tls")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.security:spring-security-messaging")

	implementation("org.slf4j:slf4j-log4j12:2.0.0-alpha1")


	implementation("io.netty:netty-all:4.1.47.Final")
	implementation("io.netty:netty:3.9.9.Final")
	implementation("org.apache.hadoop:hadoop-client:2.6.5")
	implementation("org.codehaus.janino:janino:3.0.16")
	implementation("org.codehaus.janino:commons-compiler:3.0.16")



	implementation("com.datastax.spark:spark-cassandra-connector_2.11:2.5.1")
	implementation("com.datastax.spark:spark-cassandra-connector-assembly_2.11:2.5.1")

	implementation("org.apache.spark:spark-core_2.11:2.4.7")
	implementation("org.apache.spark:spark-sql_2.11:2.4.7")
	implementation("org.apache.spark:spark-catalyst_2.11:2.4.7")
	implementation("org.apache.spark:spark-streaming_2.11:2.4.7")


//	implementation("com.datastax.oss:java-driver-core-shaded:4.7.2")
//	implementation("com.datastax.oss:java-driver-core:4.7.2")


	//Gestion cache redis
	implementation("org.springframework.data:spring-data-redis:2.4.2")
	implementation("io.lettuce:lettuce-core:6.0.1.RELEASE")



	//swagger
	implementation("io.springfox:springfox-swagger2:2.9.2")
	implementation("io.springfox:springfox-swagger-ui:2.9.2")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

