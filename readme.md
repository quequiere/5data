# 5DATA - SUPINFO

### Auteurs

- LARIBIERE Bruno - 298261
- HAUDUC Némo - 305903

### Comment démarrer le projet localement simplement et rapidement

- `Docker` et `docker-compose` doivent être installés sur la machine
- Éxecuter les commandes suivantes à la racine du dépôt GIT, le projet peut mettre quelques minutes à se lancer la première fois (si toutes données s'affichent à 0 sur le front, attendez un peu et actualiser la page) :

```bash
docker-compose up -d
```

### Accès au projet exécuté localement

- URL du Front : http://localhost
- URL du Swagger (API) : http://localhost/api/swagger-ui.html
- URL Spark Master : http://localhost:8080
- URL Spark Master : http://localhost:8081
- URL Spark Job : http://localhost:4040

### Éteindre le projet

Éxecuter la commande suivante à la racine du dépôt GIT, ceci aura pour effet d'effacer toutes les données de votre machine :

```bash
docker-compose down --volumes
```

### Aperçu du Front :

![front1.png](docs/images/front1.png)
![front2.png](docs/images/front2.png)
![front3.png](docs/images/front3.png)

### Aperçu du Swagger :

![swagger.png](docs/images/swagger.png)

### Aperçu des éléments Spark (Master, Slave et Job) :

![spark-master.png](docs/images/spark-master.png)

![spark-slave.png](docs/images/spark-slave.png)

![spark-job.png](docs/images/spark-job.png)
