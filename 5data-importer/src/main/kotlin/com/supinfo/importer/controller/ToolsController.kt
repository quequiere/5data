package com.supinfo.importer.controller

import com.supinfo.importer.services.SchoolService
import com.supinfo.importer.services.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController("/tools")
class ToolsController @Autowired constructor(val schoolService: SchoolService, val studentService: StudentService) {

    @GetMapping("/reset")
    fun deleteAllResources(): String {
        schoolService.deleteAll()
        studentService.deleteAll()
        return "done"
    }
}