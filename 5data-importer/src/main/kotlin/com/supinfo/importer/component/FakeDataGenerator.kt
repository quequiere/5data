package com.supinfo.importer.component

import com.supinfo.importer.domain.SchoolStudent
import org.springframework.stereotype.Component
import kotlin.random.Random

@Component
class FakeDataGenerator {

    fun injectFakeData(schoolStudent: SchoolStudent){

//        schoolStudent.Company = FakeCompany.values()
        schoolStudent.Company = mutableListOf<String>().apply {
            repeat(10){ this.add("AMAZON") }
            repeat(4){ this.add("FACEBOOK") }
            repeat(2){ this.add("CYBERGUN") }
            repeat(3){ this.add("COINBASE") }
            repeat(5){ this.add("CARREFOUR") }
            repeat(2){ this.add("AIRFRANCE") }
            repeat(2){ this.add("STMICROELECTRONICS") }
            repeat(2){ this.add("BAIDU") }
            repeat(8){ this.add("MICROSOFT") }
            repeat(7){ this.add("TESLA") }
            repeat(6){ this.add("APPLE") }
            repeat(1){ this.add("TOMRASYSTEMS") }
            repeat(4){ this.add("TRADINGVIEW") }
        }
                .shuffled()
                .first()

//        schoolStudent.School_Organization = SchoolOrganization.values()
        schoolStudent.School_Organization = mutableListOf<String>().apply {
            repeat(5){ this.add("SUPINFO") }
            repeat(4){ this.add("EPITECH") }
            repeat(2){ this.add("EPITA") }
        }
                .shuffled()
                .first()

//        schoolStudent.Region = Regions.values()
        schoolStudent.Region = mutableListOf<String>().apply {
            repeat(10){ this.add("CALIFORNIE") }
            repeat(8){ this.add("WISCONSIN") }
            repeat(7){ this.add("IDAHO") }
            repeat(9){ this.add("NEW_YORK") }
            repeat(7){ this.add("PENNSYLVANIE") }
            repeat(5){ this.add("TEXAS") }
            repeat(10){ this.add("FLORIDA") }
            repeat(6){ this.add("MINNESOTA") }
        }
                .shuffled()
                .first()

        schoolStudent.Pro_Contract = Random.nextBoolean()
//        if(schoolStudent.Pro_Contract) schoolStudent.Pro_Contract_Reason = ProContractReason.values()
        if(schoolStudent.Pro_Contract) schoolStudent.Pro_Contract_Reason = mutableListOf<String>().apply {
            repeat(3){ this.add("NEEDMONEY") }
            repeat(4){ this.add("WANTFASTWORK") }
            repeat(2){ this.add("COMPANYREQUIREMENT") }
        }
                .shuffled()
                .first()

//        schoolStudent.Stop_Studies = Random.nextBoolean()
        schoolStudent.Stop_Studies = Random.nextBoolean()
        if(schoolStudent.Stop_Studies) schoolStudent.Stop_Studies_Reason = mutableListOf<String>().apply {
            repeat(2){ this.add("BABIES") }
            repeat(6){ this.add("NEEDMONEY") }
            repeat(3){ this.add("FAMILYREASON") }
            repeat(8){ this.add("WRONGSTUDYTYPE") }
        }
                .toList()
                .shuffled()
                .first()

//        schoolStudent.Start_Year = Random.nextInt(2015,2017+1)
        schoolStudent.Start_Year = mutableListOf<Int>().apply {
            repeat(5){ this.add(2015) }
            repeat(6){ this.add(2016) }
            repeat(9){ this.add(2017) }
        }
                .toList()
                .shuffled()
                .first()

        when (schoolStudent.School_Organization) {
            "SUPINFO" -> schoolStudent.NbMonthsToBeHired = Random.nextInt(0,8)
            "EPITECH" -> schoolStudent.NbMonthsToBeHired = Random.nextInt(0,10)
            "EPITA" -> schoolStudent.NbMonthsToBeHired = Random.nextInt(0,12)
            else -> {
                schoolStudent.NbMonthsToBeHired = Random.nextInt(0,12)
            }
        }
    }
}

enum class StopStudiesReason{
    BABIES,
    NEEDMONEY,
    FAMILYREASON,
    WRONGSTUDYTYPE
}

enum class ProContractReason{
    NEEDMONEY,
    WANTFASTWORK,
    COMPANYREQUIREMENT
}

enum class SchoolOrganization{
    SUPINFO,
    EPITECH,
    EPITA
}

enum class FakeCompany{
    MICROSOFT,
    AMAZON,
    FACEBOOK,
    CYBERGUN,
    COINBASE,
    CARREFOUR,
    AIRFRANCE,
    STMICROELECTRONICS,
    BAIDU,
    TESLA,
    TOMRASYSTEMS,
    APPLE,
    TRADINGVIEW
}

enum class Regions{
    CALIFORNIE,
    WISCONSIN,
    IDAHO,
    NEW_YORK,
    PENNSYLVANIE,
    TEXAS,
    FLORIDA,
    MINNESOTA
}