package com.supinfo.importer.component

import com.supinfo.importer.services.SchoolService
import com.supinfo.importer.services.SchoolStudentService
import com.supinfo.importer.services.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class DataGeneratorComponent @Autowired constructor(val schoolService: SchoolService,
                                                    val studentService: StudentService,
                                                    val schoolStudentService: SchoolStudentService) {

    @PostConstruct
    fun onSpringStart() {

        studentService.tryInit()
        schoolService.tryInit()
        schoolStudentService.tryInit(schoolService.getSchoolIt(), studentService.getStudentIt())

        val students = studentService.getCount()
        val schools = schoolService.getCount()
        val schoolStudent = schoolStudentService.getCount()

        println("============================================")
        println("")
        println("")
        println("        Data importation finished")
        println("        Students: $students   Schools: $schools    SchoolStudent: $schoolStudent")
        println("")
        println("============================================")
    }
}