package com.supinfo.importer.services

import com.opencsv.bean.CsvToBeanBuilder
import com.supinfo.importer.domain.School
import com.supinfo.importer.domain.Student
import com.supinfo.importer.repository.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Files
import javax.annotation.PostConstruct

@Component
class StudentService @Autowired constructor(val studentRepository: StudentRepository) {

    fun tryInit() {
        println("Try to init database with students data")

        if (studentRepository.count() > 0) {
            print("Data base already populate, abort ...")
            return
        }

        println("Database need to be populated with students")
        importFromFile()
    }

    fun getCount(): Long {
        return studentRepository.count()
    }

    fun deleteAll(){
        studentRepository.deleteAll()
    }

    fun getStudentsFromIds(ids : List<Long>): List<Student> {
        return studentRepository.findAllById(ids).toList()
    }

    fun getStudentIt(): MutableIterable<Student> {
        return studentRepository.findAll()
    }

    fun importFromFile() {

        val reader = Files.newBufferedReader(File("./datasources/students.csv").toPath())
        val csvToBean = CsvToBeanBuilder<Student>(reader)
                .withIgnoreLeadingWhiteSpace(true)
                .withType(Student::class.java)
                .withSeparator(',')
                .build()

        val iterator = csvToBean.iterator()

        val datas = mutableListOf<Student>()
        var injected = 0L

        while (iterator.hasNext()) {
            injected++
            val line = iterator.next().apply {
                id = injected
            }
            datas.add(line)
            if(datas.size>50){
                studentRepository.saveAll(datas)
                println("$injected injected to cassandra")
                datas.clear()
            }
        }

        studentRepository.saveAll(datas)

        println("Students injected: $injected")
    }

}