package com.supinfo.importer.services

import com.supinfo.importer.component.FakeDataGenerator
import com.supinfo.importer.domain.School
import com.supinfo.importer.domain.SchoolStudent
import com.supinfo.importer.domain.Student
import com.supinfo.importer.repository.SchoolStudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import kotlin.random.Random


const val MIN_PER_SCHOOL = 5
const val MAX_PER_SCHOOL = 200
const val SCHOOL_TO_KEEP = 100

@Component
class SchoolStudentService @Autowired constructor(val schoolStudentRepository: SchoolStudentRepository,
                                                  val schoolService: SchoolService,
                                                  val studentService: StudentService,
                                                  val fakeDataGenerator: FakeDataGenerator) {


    fun tryInit(schoolIt: MutableIterable<School>, studentIt: MutableIterable<Student>): Boolean {

        val schools = schoolService.getSchoolIt()
        val students = studentService.getStudentIt()

        println("Try to init database with school data")

        if (schoolStudentRepository.count() > 0) {
            print("Data base already populate, abort ...")
            return false
        }
        if (schools.count() < 1) {
            print("School table is empty, abort")
            return false
        }
        if (students.count() < 1) {
            print("Students table is empty, abort")
            return false
        }
        println("Database need to be populated with schoolStudent data")
        importFromSchoolAndStudent(schools.toList(), students.toList())
        return true
    }


    fun getCount(): Long = schoolStudentRepository.count()


    fun importFromSchoolAndStudent(schools: List<School>, students: List<Student>) {

        var currentStudentId = -1L

        val studentsSc = schools
                .take(SCHOOL_TO_KEEP)
                .map { currentSchool ->
                    val studentQuantityToAssign = Random.nextInt(MIN_PER_SCHOOL, MAX_PER_SCHOOL+1)
                    students.take(studentQuantityToAssign)
                            .shuffled()
                            .map { currentStudent ->
                                SchoolStudent(currentStudentId++).apply {
                                    addSchoolProp(currentSchool)
                                    addStudentProp(currentStudent)
                                    fakeDataGenerator.injectFakeData(this)
                                }
                            }
                }.flatten()


        println("Saving ${studentsSc.count()} new school students please wait")

        val batchSize = 5000

        studentsSc.chunked(batchSize)
                .parallelStream()
                .forEach {
                    println("Saving ${it.size} school students")
                    schoolStudentRepository.saveAll(it)
                    println("${it.size} Saved, please wait")
                }


        println("Import studentSchool finished ${schoolStudentRepository.count()}")

    }
}