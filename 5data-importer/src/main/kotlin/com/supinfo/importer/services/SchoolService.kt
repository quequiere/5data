package com.supinfo.importer.services

import com.opencsv.bean.CsvToBeanBuilder
import com.supinfo.importer.domain.School
import com.supinfo.importer.domain.Student
import com.supinfo.importer.repository.SchoolRepository
import com.supinfo.importer.repository.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Files
import javax.annotation.PostConstruct

@Component
class SchoolService @Autowired constructor(val schoolRepository: SchoolRepository, val studentService: StudentService) {


    fun tryInit(): Boolean {
        println("Try to init database with school data")

        if (schoolRepository.count() > 0) {
            print("Data base already populate, abort ...")
            return false
        }

        println("Database need to be populated with schools")
        importFromFile()
        return true
    }


    fun getCount() : Long = schoolRepository.count()


    fun saveSchool(school : School): School {
        return schoolRepository.save(school)
    }

    fun getSchoolIt(): MutableIterable<School> {
        return schoolRepository.findAll()
    }

    fun deleteAll(){
        schoolRepository.deleteAll()
    }

    fun importFromFile() {

        val reader = Files.newBufferedReader(File("./datasources/schools.csv").toPath())
        val csvToBean = CsvToBeanBuilder<School>(reader)
                .withIgnoreLeadingWhiteSpace(true)
                .withType(School::class.java)
                .withSeparator(',')
                .build()

        val iterator = csvToBean.iterator()

        val datas = mutableListOf<School>()
        var imported = 0L

        while (iterator.hasNext()) {
            imported++
            if (imported % 500L == 0L) println("$imported school imported")
            datas.add(iterator.next())
        }

        println("Total school imported $imported")
        println("Reducing data")

        val reworked = datas.groupBy { it.SCHOOL_NUMBER }
                .values
                .mapIndexed { index, list ->  list.first().apply { this.id=index.toLong() }}

        println("Data reduced to ${reworked.size}")

        println("Saving data to noSql")
        schoolRepository.saveAll(reworked)
        println("Data saved to no sql")

    }

}