package com.supinfo.importer.domain

import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table

@Table("Schools")
data class School (
    @PrimaryKey
    var id : Long = -1,
    val PUBLIC_OR_INDEPENDENT :String = "",
    val DISTRICT_NUMBER :String = "",
    val DISTRICT_NAME :String = "",
    val SCHOOL_NUMBER :String = "",
    val SCHOOL_NAME :String = ""
)