package com.supinfo.importer.domain

import org.springframework.data.cassandra.core.mapping.Indexed
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table

@Table("SchoolStudent")
data class SchoolStudent(

        @PrimaryKey
        var id: Long,
        var school_number: String = "",
        var school_name: String = "",
        var district_number: String = "",
        var district_name: String = "",
        @Indexed
        var public_or_independant: String = "",
        @Indexed
        var sex: String = "",
        var age: Int = 0,
        var address: String = "",
        var famsize: String = "",
        var Pstatus: String = "",
        var Medu: Int = 0,
        var Fedu: Int = 0,
        var Mjob: String = "",
        var Fjob: String = "",
        var reason: String = "",
        var guardian: String = "",
        @Indexed
        var traveltime: Int = 0,
        var studytime: Int = 0,
        var failures: Int = 0,
        var schoolsup: String = "",
        var famsup: String = "",
        var paid: String = "",
        var activities: String = "",
        var nursery: String = "",
        var higher: String = "",
        var internet: String = "",
        var romantic: String = "",
        var famrel: Int = 0,
        var freetime: Int = 0,
        var goout: Int = 0,
        var Dalc: Int = 0,
        var Walc: Int = 0,
        var health: Int = 0,
        var absences: Int = 0,
        @Indexed
        var G1: Int = 0,
        @Indexed
        var G2: Int = 0,
        @Indexed
        var G3: Int = 0,

        //Auto generated values

        var NbMonthsToBeHired: Int = 0,
        @Indexed
        var Company: String = "",
        @Indexed
        var School_Organization: String = "",
        @Indexed
        var Region: String = "",
        @Indexed
        var Pro_Contract: Boolean = false,
        @Indexed
        var Pro_Contract_Reason: String? = null,
        @Indexed
        var Stop_Studies: Boolean = false,
        @Indexed
        var Stop_Studies_Reason: String? = null,
        @Indexed
        var Start_Year: Int = 0


) {
    fun addSchoolProp(school: School) {
        this.school_number = school.SCHOOL_NUMBER
        this.school_name = school.SCHOOL_NAME
        this.district_number = school.DISTRICT_NUMBER
        this.district_name = school.DISTRICT_NAME
        this.public_or_independant = school.PUBLIC_OR_INDEPENDENT
    }

    fun addStudentProp(studentFromSchool: Student) {
        this.sex = studentFromSchool.sex
        this.age = studentFromSchool.age
        this.address = studentFromSchool.address
        this.famsize = studentFromSchool.famsize
        this.Pstatus = studentFromSchool.Pstatus
        this.Medu = studentFromSchool.Medu
        this.Fedu = studentFromSchool.Fedu
        this.Mjob = studentFromSchool.Mjob
        this.Fjob = studentFromSchool.Fjob
        this.reason = studentFromSchool.reason
        this.guardian = studentFromSchool.guardian
        this.traveltime = studentFromSchool.traveltime
        this.studytime = studentFromSchool.studytime
        this.failures = studentFromSchool.failures
        this.schoolsup = studentFromSchool.schoolsup
        this.famsup = studentFromSchool.famsup
        this.paid = studentFromSchool.paid
        this.activities = studentFromSchool.activities
        this.nursery = studentFromSchool.nursery
        this.higher = studentFromSchool.higher
        this.internet = studentFromSchool.internet
        this.romantic = studentFromSchool.romantic
        this.famrel = studentFromSchool.famrel
        this.freetime = studentFromSchool.freetime
        this.goout = studentFromSchool.goout
        this.Dalc = studentFromSchool.Dalc
        this.Walc = studentFromSchool.Walc
        this.health = studentFromSchool.health
        this.absences = studentFromSchool.absences
        this.G1 = studentFromSchool.G1
        this.G2 = studentFromSchool.G2
        this.G3 = studentFromSchool.G3
    }
}