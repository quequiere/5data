package com.supinfo.importer.domain

import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table

@Table("Students")
data class Student(

        @PrimaryKey
        var id: Long = -1,
        val school: String ="",
        val sex: String ="",
        val age: Int =0,
        val address: String ="",
        val famsize: String ="",
        val Pstatus: String ="",
        val Medu: Int =0,
        val Fedu: Int =0,
        val Mjob: String ="",
        val Fjob: String ="",
        val reason: String ="",
        val guardian: String ="",
        val traveltime: Int =0,
        val studytime: Int =0,
        val failures: Int =0,
        val schoolsup: String ="",
        val famsup: String ="",
        val paid: String ="",
        val activities: String ="",
        val nursery: String ="",
        val higher: String ="",
        val internet: String ="",
        val romantic: String ="",
        val famrel: Int =0,
        val freetime: Int =0,
        val goout: Int =0,
        val Dalc: Int =0,
        val Walc: Int =0,
        val health: Int =0,
        val absences: Int =0,
        val G1: Int =0,
        val G2: Int =0,
        val G3: Int=0
)