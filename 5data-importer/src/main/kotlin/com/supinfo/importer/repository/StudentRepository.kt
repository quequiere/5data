package com.supinfo.importer.repository

import com.supinfo.importer.domain.Student
import org.springframework.data.repository.CrudRepository

interface StudentRepository : CrudRepository<Student, Long>