package com.supinfo.importer.repository

import com.supinfo.importer.domain.School
import com.supinfo.importer.domain.SchoolStudent
import com.supinfo.importer.domain.Student
import org.springframework.data.repository.CrudRepository

interface SchoolStudentRepository : CrudRepository<SchoolStudent, Long>