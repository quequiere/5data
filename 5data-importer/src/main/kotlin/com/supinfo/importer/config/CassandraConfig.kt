package com.supinfo.importer.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration
import org.springframework.data.cassandra.config.CqlSessionFactoryBean
import org.springframework.data.cassandra.config.SchemaAction
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification
import org.springframework.data.cassandra.core.cql.keyspace.DropKeyspaceSpecification
import java.util.*


/**
 * See https://github.com/springframeworkguru/spring-boot-cassandra-example/blob/master/src/main/java/guru/springframework/config/CassandraConfig.java
 */
@Configuration
class CassandraConfig : AbstractCassandraConfiguration() {

    val KEYSPACE = "dataspace"

    @Value("\${spring.data.cassandra.contact-points}")
    lateinit var cassandraHost : String

    override fun cassandraSession(): CqlSessionFactoryBean {
        return super.cassandraSession().apply {
            setContactPoints(cassandraHost)
        }
    }

    override fun getSchemaAction(): SchemaAction {
        return SchemaAction.CREATE_IF_NOT_EXISTS
    }

     override fun getKeyspaceCreations(): MutableList<CreateKeyspaceSpecification> {
        val specification = CreateKeyspaceSpecification.createKeyspace(KEYSPACE).ifNotExists()
        return mutableListOf(specification)
    }

     override fun getKeyspaceDrops(): MutableList<DropKeyspaceSpecification> {
//        return mutableListOf(DropKeyspaceSpecification.dropKeyspace(KEYSPACE))
         return mutableListOf()
    }

     override fun getKeyspaceName(): String {
        return KEYSPACE
    }

    override fun getEntityBasePackages(): Array<out String> {
        return arrayOf("com.supinfo.importer.domain")
    }
}